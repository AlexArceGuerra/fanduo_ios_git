//
//  LoginResponseDatos.swift
//  FD
//
//  Created by Juan francisco Cordoba on 21/1/17.
//  Copyright © 2017 keekorok. All rights reserved.
//

import Foundation


class LoginResponseDatos {

    let codigoCtr : String?
    let idUsuario : String?
    let email : String?
    let pass : String?
    let telefono : String?
    let nombre : String?
    let apellidos : String?
    let edad : String?
    let ciudad : String?
    
    
    init (codigoCtr:String?, idUsuario:String?, email:String?, pass:String?, telefono:String?, nombre:String?, apellidos:String?, edad:String?, ciudad:String?){
    
        
        self.codigoCtr      = codigoCtr
        self.idUsuario      = idUsuario
        self.email          = email
        self.pass           = pass
        self.telefono       = telefono
        self.nombre         = nombre
        self.apellidos      = apellidos
        self.edad           = edad
        self.ciudad         = ciudad
    
    }
    
    
}


