//
//  RegistroViewController.swift
//  FD
//
//  Created by Alejandro Arce on 24/1/17.
//  Copyright © 2017 keekorok. All rights reserved.
//

import UIKit
import QuartzCore



class RegistroViewController: UIViewController, UITextFieldDelegate {

    enum EnumeradoValidaciones {
        case validacionCorrecta
        case emailNoValido
        case emailEsObligatorio
        case passNoValida
        case passEsObligatoria
        case contraseniasNoCoinciden
        case contraseniaLongitudMenor
        case condicionesNoAceptadas
        case telefObligatorio
        case nombreObligatorio
        case apellidosObligatorio
        case ciudadObligatorio
        case edadObligatorio
        
    }
    
    
    @IBOutlet weak var ch: UIButton!
    @IBOutlet weak var registroEmailUsuario: UITextField!
    @IBOutlet weak var registroPassUsuario: UITextField!
    @IBOutlet weak var registroConfirmacionPassUsuario: UITextField!
    @IBOutlet weak var registroTelefonoUsuario: UITextField!
    @IBOutlet weak var registroNombreUsuario: UITextField!
    @IBOutlet weak var registroApellidosUsuario: UITextField!
    @IBOutlet weak var registroCiudadUsuario: UITextField!
    @IBOutlet weak var registroEdadUsuario: UITextField!
    @IBOutlet weak var btnLeerCondiciones: UIButton!
    @IBOutlet weak var loading: UIActivityIndicatorView!

    
    let alerta = Alerta()
    let dataProvider = ConnectionLocalService()
    var aceptaCondiciones = Bool()
    var esPantallaLeerCG = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnLeerCondiciones.setTitle("Leer Condiciones", for: .normal)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if esPantallaLeerCG {
            mostrarDatosUsuario()
            comprobarValorEleccion(valor: aceptaCondiciones)
        }
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    //MARK: UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if let texto = textField.text {
        
            if texto.characters.count == 0 {
            
                cambiarColorBordeDefecto(textField)
            
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        switch textField.tag {
        case 1,4,8:
            
            if let texto = textField.text {
                
                if texto.characters.count == 0 {
                    
                    cambiarColorBordeDefecto(textField)
                    
                }
            }
        
        default:
            
            cambiarColorBordeDefecto(textField)
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text: NSString = (textField.text ?? "") as NSString
        let resultString = text.replacingCharacters(in: range, with: string)
        
        switch textField.tag {
        case 1:
            if !esEmailValido(emailAddressString: resultString) {
                cambiarColorBordeError(textField)
            }else{
                cambiarColorBordeDefecto(textField)
            }
        case 4:
            if !esNumeroTelefonoValido(numero: resultString){
                cambiarColorBordeError(textField)
            }else{
                cambiarColorBordeDefecto(textField)
            }
        case 8:
            if !esEdadValido(edadUsuaio: resultString){
                cambiarColorBordeError(textField)
            }else{
                cambiarColorBordeDefecto(textField)
            }
            
        default:
            textField.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).cgColor
        }
        
        return true
    }
    
    
    //MARK: Actions
    @IBAction func backButton(_ sender: Any) {
        //dismiss(animated: true, completion: nil)
        self.performSegue(withIdentifier: "logout", sender: self)
        
    }
    
    @IBAction func checkButton(_ sender: Any) {
        let paso = sender as! UIButton
        paso.isSelected = !paso.isSelected
        aceptaCondiciones = paso.isSelected
    }
    
    @IBAction func btnRegistro(_ sender: UIButton) {
        
        validarDatosCorrectos()
    }
    
    @IBAction func irAPantallaLeerCondiciones(_ sender: Any) {
        
        obtenerDatosUsuario()
    }
    
    //MARK: Metodos validaciones.
    
    func validarDatosCorrectos(){
    
        
        var datosCorrectos : EnumeradoValidaciones
        
        datosCorrectos = .validacionCorrecta
        
        //comentar test
//        comprobarCodigoError(tipo: datosCorrectos)
//        return
        
     
        if let textoEmail = registroEmailUsuario.text {
        
            if textoEmail.characters.count == 0 {
                datosCorrectos = .emailEsObligatorio
                comprobarCodigoError(tipo: datosCorrectos)
                return
            }
            
            
            if !esEmailValido(emailAddressString: textoEmail){
                datosCorrectos = .emailNoValido
                comprobarCodigoError(tipo: datosCorrectos)
                return
            }
        
        }
        
        if let pass = registroPassUsuario.text {
        
            if pass.characters.count == 0 {
                datosCorrectos = .passEsObligatoria
                return comprobarCodigoError(tipo: datosCorrectos)
                
            }
            
            if pass.characters.count < 7 {
                datosCorrectos = .contraseniaLongitudMenor
                return comprobarCodigoError(tipo: datosCorrectos)
            }
            
            if let passConfirmacion = registroConfirmacionPassUsuario.text {
                
                if !comprobarSiContraseniaContieneUnNumero(contrasenia: pass) {
                        datosCorrectos = .passNoValida
                        comprobarCodigoError(tipo: datosCorrectos)
                        return
                    
                }
                
                if !comprobarConfirmacionContrasenia(contrasenia: pass, textoVerificarContrasenia: passConfirmacion) {
                    datosCorrectos = .contraseniasNoCoinciden
                    comprobarCodigoError(tipo: datosCorrectos)
                    return
                    
                }
                
            }
        
        }
        
        
        
        // telefono
        if let textTelf = registroTelefonoUsuario.text {
            
            if (textTelf.characters.count != 9)  {
                datosCorrectos = .telefObligatorio
                comprobarCodigoError(tipo: datosCorrectos)
                return
            }
        }
        //nombre
        if let textNom = registroNombreUsuario.text {
            if textNom.characters.count == 0 {
                datosCorrectos = .nombreObligatorio
                comprobarCodigoError(tipo: datosCorrectos)
                return
            }
        }
        //apellidos
        if let textApell = registroApellidosUsuario.text {
            if textApell.characters.count == 0 {
                datosCorrectos = .apellidosObligatorio
                comprobarCodigoError(tipo: datosCorrectos)
                return
            }
        }
        //ciudad
        if let textCiudad = registroCiudadUsuario.text {
            if textCiudad.characters.count == 0 {
                datosCorrectos = .ciudadObligatorio
                comprobarCodigoError(tipo: datosCorrectos)
                return
            }
        }
        //edad
        if let textEdad = registroEdadUsuario.text {
            if ((textEdad.characters.count == 0) || (textEdad.characters.count > 2)){
                datosCorrectos = .edadObligatorio
                comprobarCodigoError(tipo: datosCorrectos)
                return
            }
        }
        // condiciones
        if !aceptaCondiciones {
            datosCorrectos = .condicionesNoAceptadas
            comprobarCodigoError(tipo: datosCorrectos)
            
        }

        
        comprobarCodigoError(tipo: datosCorrectos)
        
    }
    
    
    func comprobarCodigoError(tipo:EnumeradoValidaciones) {
        switch tipo {
        case .emailEsObligatorio:
            self.alerta.mostrarAlerta(targetVC: self, titulo: "Error", mensaje: "El email es obligatorio")
            cambiarColorBordeError(registroEmailUsuario)
        case .emailNoValido:
            self.alerta.mostrarAlerta(targetVC: self, titulo: "Error", mensaje: "El email no es valido")
            cambiarColorBordeError(registroEmailUsuario)
        case .passEsObligatoria:
            self.alerta.mostrarAlerta(targetVC: self, titulo: "Error", mensaje: "La contraseña es obligatoria")
            cambiarColorBordeError(registroPassUsuario)
            cambiarColorBordeError(registroConfirmacionPassUsuario)
        case .contraseniaLongitudMenor:
            self.alerta.mostrarAlerta(targetVC: self, titulo: "Error", mensaje: "La contraseña debe tener al menos 8 caracteres")
        case .passNoValida:
            self.alerta.mostrarAlerta(targetVC: self, titulo: "Error", mensaje: "La contraseña debe tener al menos un número y una letra")
            cambiarColorBordeError(registroPassUsuario)
        case .contraseniasNoCoinciden:
            self.alerta.mostrarAlerta(targetVC: self, titulo: "Error", mensaje: "Las contraseñas no coinciden")
            cambiarColorBordeError(registroPassUsuario)
            cambiarColorBordeError(registroConfirmacionPassUsuario)
        case .condicionesNoAceptadas:
            self.alerta.mostrarAlerta(targetVC: self, titulo: "Error", mensaje: "Debe aceptar las condiciones")
        
        case .telefObligatorio:
            self.alerta.mostrarAlerta(targetVC: self, titulo: "Error", mensaje: "El teléfono debe tener 9 dígitos")
        case .nombreObligatorio:
            self.alerta.mostrarAlerta(targetVC: self, titulo: "Error", mensaje: "El nombre es obligatorio")
        case .apellidosObligatorio:
            self.alerta.mostrarAlerta(targetVC: self, titulo: "Error", mensaje: "Los apllidos son obligatorios")
        case .ciudadObligatorio:
            self.alerta.mostrarAlerta(targetVC: self, titulo: "Error", mensaje: "La ciudad es obligatoria")
        case .edadObligatorio:
            self.alerta.mostrarAlerta(targetVC: self, titulo: "Error", mensaje: "La edad no es correcta")

        
        
        case .validacionCorrecta:
//            iniciarLoading()
            self.realizarRegistro()
            
        }
    }
    
    func esEmailValido(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    
    func esNumeroTelefonoValido(numero:String) -> Bool {
        
        if numero.characters.count > 1 && numero.characters.count < 10 && numero.characters.count == 9  {
            return true
        }else{
            return false
        }
    }
    
    func esEdadValido(edadUsuaio:String) -> Bool {
        
        if edadUsuaio.characters.count > 2 {
            return false
        }else{
            return true
        }
        
    }
    
    
    func comprobarConfirmacionContrasenia(contrasenia:String, textoVerificarContrasenia:String) -> Bool {
        if contrasenia == textoVerificarContrasenia {
            return true
        }else{
            return false
        }
    }
    
    func comprobarSiContraseniaContieneUnNumero(contrasenia:String) -> Bool {
        
        let numberCharacters = NSCharacterSet.decimalDigits
        let letterCharacter = NSCharacterSet.letters
        
        if contrasenia.rangeOfCharacter(from: numberCharacters) != nil {
            
            if contrasenia.rangeOfCharacter(from: letterCharacter) != nil {
            
                return true
            
            }else{
                return false
            }
            
        }else{
            return false
        }
    }
    
    //MARK: Métodos
    func cambiarColorBordeError(_ textfield: UITextField) {
        textfield.layer.borderWidth = 1.0
        textfield.layer.cornerRadius = 5.0
        textfield.layer.borderColor = UIColor.red.cgColor
    }

    func cambiarColorBordeDefecto(_ textfield: UITextField) {
    
        textfield.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).cgColor
    
    }
    
    func comprobarValorEleccion(valor: Bool){
    
        switch valor {
        case false:
            cambiarEstadoCheckCondiciones(text: "Rechazo las condiciones", valor: false)
        case true:
            cambiarEstadoCheckCondiciones(text: "Acepto las condiciones", valor: true)
        }
    
    }
    
    func cambiarEstadoCheckCondiciones(text:String, valor: Bool) {
    
        btnLeerCondiciones.setTitle(text, for: .normal)
        btnLeerCondiciones.setTitleColor(UIColor.lightGray, for: .normal)
        ch.isSelected = valor
        aceptaCondiciones = valor
    }
    
    
    func mostrarDatosUsuario(){
    
        if let datosRegistro = UserDefaults.standard.object(forKey: "datosUsuario") as? [String:String] {
        
            if let email = datosRegistro["emailUsuario"] {
                registroEmailUsuario.text = email
            }
            
            if let pass = datosRegistro["passUsuario"] {
                registroPassUsuario.text = pass
            }
            
            if let passConfirm = datosRegistro["passConfirmUsuario"] {
                registroConfirmacionPassUsuario.text = passConfirm
            }
            
            if let telefono = datosRegistro["telefonoUsuario"] {
                registroTelefonoUsuario.text = telefono
            }
            
            if let nombre = datosRegistro["nombreUsuario"] {
                registroNombreUsuario.text = nombre
            }
            
            if let apellidos = datosRegistro["apellidosUsuario"] {
                registroApellidosUsuario.text = apellidos
            }
            
            if let ciudad = datosRegistro["ciudadUsuario"] {
                registroCiudadUsuario.text = ciudad
            }
            
            if let edad = datosRegistro["edadUsuario"] {
                registroEdadUsuario.text = edad
            }
            
        }
        
    }
    
    func obtenerDatosUsuario(){
    
        var datosRegistro = [String:String]()
        datosRegistro["emailUsuario"] = registroEmailUsuario.text
        datosRegistro["passUsuario"] = registroPassUsuario.text
        datosRegistro["passConfirmUsuario"] = registroConfirmacionPassUsuario.text
        datosRegistro["telefonoUsuario"] = registroTelefonoUsuario.text
        datosRegistro["nombreUsuario"] = registroNombreUsuario.text
        datosRegistro["apellidosUsuario"] = registroApellidosUsuario.text
        datosRegistro["ciudadUsuario"] = registroCiudadUsuario.text
        datosRegistro["edadUsuario"] = registroEdadUsuario.text
        
        let userDefault = UserDefaults.standard
        userDefault.set(datosRegistro, forKey: "datosUsuario")
        
    }
    
    
    func realizarRegistro(){
//        iniciarLoading()
        self.alerta.iniciarLoading(targetVC: self, mensaje: "Registrando...")
        
        dataProvider.realizarRegistro(email: registroEmailUsuario.text!, pass: registroPassUsuario.text!, telefono: registroTelefonoUsuario.text!, nombre: registroNombreUsuario.text!, apellidos: registroApellidosUsuario.text!, ciudad: registroCiudadUsuario.text!, edad: registroEdadUsuario.text!, tipoAccion: "Registro"){ usuario in
            
//            self.pararLoading()
            if let usuario = usuario {
                
                if let codigo = usuario.codigoCtr {
//                    self.pararLoading()

                    self.alerta.pararLoading(targetVC: self)
                
                    self.comprobarCodigoLogin(codigo: codigo)
                    
                }else{
                    self.alerta.pararLoading(targetVC: self)

//                    self.pararLoading()

//                    self.alerta.mostrarAlertaSinServicio(targetVC: self)
                }
                
            }else{
                self.alerta.pararLoading(targetVC: self)

//                self.pararLoading()

//                self.alerta.mostrarAlertaSinServicio(targetVC: self)
                
            }
        
        }
        
//        pararLoading()
    
    }

    func comprobarCodigoLogin (codigo:String) {
        
        switch codigo {
            
        case "Registered", "Added":
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "ListadoVC") as! CancionesViewController
            self.present(newViewController, animated: true, completion: nil)
            
        case "InvalidPass":
            self.alerta.mostrarAlerta(targetVC: self, titulo: "Error", mensaje: "Contraseña inválida")
            
        case "NotRegister":
            self.alerta.mostrarAlerta(targetVC: self, titulo: "Error", mensaje: "Usuario no registrado. Debe registrarse primero.")
        
        case "NeedRegister":
            self.alerta.mostrarAlerta(targetVC: self, titulo: "Error", mensaje: "Usuario no registrado. Debe registrarse primero.")
        
        default:
            self.alerta.mostrarAlertaSinServicio(targetVC: self)
        
        }
        
    }
    
    
//    func iniciarLoading(){
//        
////        loading.isHidden = false
////        loading.startAnimating()
//        
//        
//        
//        
//            // activity
//            let overlay = UIView(frame: self.view.frame)
//            overlay.center = self.view.center
//            
//            overlay.alpha = 0
//            overlay.backgroundColor = UIColor.black
//            overlay.tag = 100
//            self.view.addSubview(overlay)
//            self.view.bringSubview(toFront: overlay)
//            
//            UIView .animate(withDuration: 0.5) {
//                UIView.setAnimationDuration(0.5)
//                overlay.alpha = 0.8
//                
//            }
//            let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
//            indicator.center = overlay.center
//            indicator.startAnimating()
//            overlay.addSubview(indicator)
//            let label = UILabel()
//            label.text = "Registrando..."
//            label.textColor = UIColor.white
//            label.sizeToFit()
//            label.center = CGPoint(x: indicator.center.x, y: indicator.center.y + 30)
//            overlay.addSubview(label)
//        
////        UIApplication.shared.beginIgnoringInteractionEvents()
//        
//    }
//    
//    func pararLoading(){
//        
//        if let viewWithTag = self.view.viewWithTag(100) {
//            viewWithTag.removeFromSuperview()
//
//        }
    
//        loading.stopAnimating()
//        UIApplication.shared.endIgnoringInteractionEvents()
        
//    }
}
