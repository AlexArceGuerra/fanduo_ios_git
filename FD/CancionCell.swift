//
//  CancionCell.swift
//  FD
//
//  Created by Juan francisco Cordoba on 20/1/17.
//  Copyright © 2017 keekorok. All rights reserved.
//

import UIKit
import AVFoundation
protocol CancionCellDelegate {
    func test()
    func currPlay(currAVPlayer: AVPlayer)
    func cantarWasTuoched(cancion: Cancion)
}


var playerAudio : AVPlayer!

class CancionCell: UITableViewCell {
    
    @IBOutlet weak var caratulaAutor: UIImageView!
    @IBOutlet weak var tituloCancion: UILabel!
    @IBOutlet weak var nombreArtista: UILabel!
    @IBOutlet weak var botonCantar: UIButton!
    @IBOutlet weak var botonPreview: UIButton!
    
    
    var urlCancion = String()
    let alerta = Alerta()
    var isPreViewPlay : Bool!
//    var idCancion = String()
    
    var cancion : Cancion!
    
    
    var delegate: CancionCellDelegate?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    
    @IBAction func cantarAction(_ sender: Any) {
        
        delegate?.cantarWasTuoched(cancion: self.cancion)
    }
    
    
    @IBAction func previewAction2(_ sender: Any) {
        self .previewAction(sender)
    }
    @IBAction func previewAction(_ sender: Any) {
        let currentSoundActive =  UserDefaults.standard.bool(forKey: "SoundActive")
        if (currentSoundActive &&  !botonPreview.isSelected){
            return
        }
        if botonPreview.isSelected{
            botonPreview.isSelected = false
            UserDefaults.standard.set(false, forKey: "SoundActive")

        }else{
            botonPreview.isSelected = true
            UserDefaults.standard.set(true, forKey: "SoundActive")
            let audioUrl = NSURL(string: self.urlCancion)
            playerAudio = AVPlayer(url: audioUrl! as URL)
        }
        fijarBotonReproductor(valor: botonPreview.isSelected)
        reproducirUrlAudioMini(valor: botonPreview.isSelected)
    }
    
    
    func fijarBotonReproductor(valor:Bool){
        if valor {
            
            botonPreview.setImage(UIImage(named: "pause Filled"), for: .selected)
            
        }else {
            botonPreview.setImage(UIImage(named: "playFilled"), for: .normal)
        }
    }
    
    func reproducirUrlAudioMini(valor:Bool) {
  

        if !valor{

            pausar()
//            playerAudio.pause()
        }else{
//            DispatchQueue.main.async { [unowned self] in
            
           
            NotificationCenter.default.addObserver(self, selector: #selector(pausar),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerAudio.currentItem)
            
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            }
            catch {
                // report for an error
            }
            playerAudio.seek(to: kCMTimeZero, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
             playerAudio.play()
            self.botonCantar.alpha = 0.1
            delegate?.currPlay(currAVPlayer: playerAudio)
            
            
            
        }
    }
    
    
    
    
    
    func pausar(){
        
        botonPreview.setImage(UIImage(named: "playFilled"), for: .selected)
        botonPreview.isSelected = false

        UserDefaults.standard.set(false, forKey: "SoundActive")
        NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: nil)
        playerAudio.pause()
        self.botonCantar.alpha = 1

        
    }
    
}
