//
//  ConnectionRemoteService.swift
//  FD
//
//  Created by Juan francisco Cordoba on 20/12/16.
//  Copyright © 2016 keekorok. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


class ConnectionRemoteService{
    

    // produccion
//    let urlBase = "http://www.fanduo.info:8080/"
    // desarrollo
//    let urlBase = "http://81.45.66.119:8080"
    
    // AWS
     let urlBase = "http://34.250.208.108/"
    
    
    
    
    
    func ExisteVideo(usuario: String, cancion: String, completionHandler: @escaping (_ String:String?) -> Void){
        
        let url = URL(string:  urlBase + "fanduo/rest/ExistVideo")!
        var arrayParametros : Parameters
        
         arrayParametros = ["idUsuario":usuario  , "idCancion": cancion]
        
        
        Alamofire.request(url, method: .get, parameters: arrayParametros, encoding: URLEncoding.default, headers: nil).validate().responseJSON() { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    let result = json["video"].stringValue
                    
                    completionHandler(result)
                    
                }
            case .failure(let error):
                print(error)
                completionHandler(nil)
                
            }
            
        }
        
        
    }

    
    
    
    
    
   
    
    
    func realizarLogin(usuario:String, pass:String, tipoAccion:String, completionHandler: @escaping ([String:String]?) -> Void){
    
        
        let url = URL(string: urlBase + "fanduo/rest/RegistroServ")!
        
        
        let parametros:Parameters = ["email":usuario, "pass":pass, "tipoAccion":tipoAccion]
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        Alamofire.request(url, method: .post, parameters: parametros, encoding: JSONEncoding.default, headers: headers).validate().responseJSON() { response in
            
            switch response.result {
            
            case .success:
                
                if let value = response.result.value {
                
                    let json = JSON(value)
                
                    var login = [String:String]()
                    
                    login["codigoCtr"] = json["usuario"]["codigoCtr"].stringValue
                    login["idUsuario"] = json["usuario"]["idUsuario"].stringValue
                    login["email"] = json["usuario"]["email"].stringValue
                    login["pass"] = json["usuario"]["pass"].stringValue
                    login["telefono"] = json["usuario"]["telefono"].stringValue
                    login["nombre"] = json["usuario"]["nombre"].stringValue
                    login["apellidos"] = json["usuario"]["apellidos"].stringValue
                    login["edad"] = json["usuario"]["edad"].stringValue
                    login["ciudad"] = json["usuario"]["ciudad"].stringValue
                    
                    completionHandler(login)
                }
                
            case .failure(let error):
                
                print(error)
                completionHandler(nil)
            
            }
            
        }
        
    }
    
    
    
    func upLoadVideo(idUsuario:String, idCancion:String, urlCancionOrig:String,  urlCancionUser:String, estado1:String, completionHandler: @escaping (_ String:String?) -> Void){
    
        let url = URL(string: urlBase + "fanduo/rest/UploadVideo")!
        
        let parametros:Parameters = ["idUsuario":idUsuario, "idCancion":idUsuario  , "urlCancionOrig":urlCancionOrig, "urlCancionUser":urlCancionUser, "estado1":estado1, ]
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        Alamofire.request(url, method: .post, parameters: parametros, encoding: JSONEncoding.default, headers: headers).validate().responseJSON() { response in
            
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    let result = json["video"].stringValue
                    
                    completionHandler(result)
                    
                }
            case .failure(let error):
                print(error)
                completionHandler(nil)
                
            }

            
        }
        
    
    }
    
    
    
    
    func realizarRegistro(usuario:String, pass:String, telefono:String, nombre:String, apellidos:String, ciudad:String, edad:String, tipoAccion:String, completionHandler: @escaping ([String:String]?) -> Void){
        
        let url = URL(string: urlBase + "fanduo/rest/RegistroServ")!
        
        let parametros:Parameters = ["email":usuario, "pass":pass, "telefono":telefono, "nombre":nombre, "apellidos":apellidos, "ciudad":ciudad, "edad": edad, "tipoAccion":tipoAccion]
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        
        Alamofire.request(url, method: .post, parameters: parametros, encoding: JSONEncoding.default, headers: headers).validate().responseJSON() { response in
            
            switch response.result {
                
            case .success:
                
                if let value = response.result.value {
                    
                    let json = JSON(value)
                    
                    var login = [String:String]()
                    
                    login["codigoCtr"] = json["usuario"]["codigoCtr"].stringValue
                    login["idUsuario"] = json["usuario"]["idUsuario"].stringValue
                    login["email"] = json["usuario"]["email"].stringValue
                    login["pass"] = json["usuario"]["pass"].stringValue
                    login["telefono"] = json["usuario"]["telefono"].stringValue
                    login["nombre"] = json["usuario"]["nombre"].stringValue
                    login["apellidos"] = json["usuario"]["apellidos"].stringValue
                    login["edad"] = json["usuario"]["edad"].stringValue
                    login["ciudad"] = json["usuario"]["ciudad"].stringValue
                    
                    completionHandler(login)
                }
                
            case .failure(let error):
                
                print(error)
                completionHandler(nil)
                
            }
            
        }
    
    }


    func getListaCanciones(activo: String?, dispositivo: String, completionHandler: @escaping ([[String:String]]?) -> Void){
        
        let url = URL(string:  urlBase + "fanduo/rest/listacanciones")!
        var arrayParametros : Parameters 
        
        if let activo = activo {
        
            arrayParametros = ["activo":activo, "device": dispositivo]
            
        }else{
        
            arrayParametros = ["device": dispositivo]
        
        }

        Alamofire.request(url, method: .get, parameters: arrayParametros, encoding: URLEncoding.default, headers: nil).validate().responseJSON() { response in
        
            switch response.result {
            
            case .success:
                
                if let value = response.result.value {
                
                    let json = JSON(value)
                    var result = [[String:String]]()
                    let datos = json["canciones"].arrayValue
                    
                    for dato in datos {
                    
                        var cancion = [String:String]()
                        cancion["idCancion"] = dato["idCancion"].stringValue
                        cancion["activo"] = dato["activo"].stringValue
                        cancion["concurso"] = dato["concurso"].stringValue
                        cancion["imagenMini"] = dato["imagenMini"].stringValue
                        cancion["urlVideoBase"] = dato["urlVideoBase"].stringValue
                        cancion["urlVideoTexto"] = dato["urlVideoTexto"].stringValue
                        cancion["urlAudioMini"] = dato["urlAudioMini"].stringValue
                        cancion["autor"] = dato["autor"].stringValue
                        cancion["tema"] = dato["tema"].stringValue
                        cancion["var1"] = dato["var1"].stringValue
                        cancion["var2"] = dato["var2"].stringValue
                        cancion["var3"] = dato["var3"].stringValue
                        result.append(cancion)
                    
                    }
                    
                    completionHandler(result)
                
                }
                
            case .failure(let error):
                
                print(error)
                completionHandler(nil)
            
            }
        
        }
        
    
    }


}
