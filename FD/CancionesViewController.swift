//
//  CancionesViewController.swift
//  AVCam Swift
//
//  Created by Alejandro Arce on 1/1/17.
//  Copyright © 2017 Apple, Inc. All rights reserved.
//

import UIKit
import Kingfisher
import AVFoundation






class CancionesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,CancionCellDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    
    var canciones : [Cancion] = [Cancion]()
//    var cancionesOrdenadas : [Cancion] = [Cancion]()
    var cancionSeleccionada : Cancion!
    let dataProvider = ConnectionLocalService()
    let alerta = Alerta()
    var playerAudio : AVPlayer!
    var nuRowsCanciones = 4
    var currentAVPlayer : AVPlayer!
    var currIdUsuario : String!
    var uploadAllowed  = true

//    let getRemoteService = ConnectionRemoteService()


    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        //Se añade un notificationCenter para comprobar que cuando la app se vaya a segundo plano en esta pantalla. Si vuelve que se pida lista de canciones. refrescarListaCanciones
        
        NotificationCenter.default.addObserver(self, selector: #selector(pedirListaCanciones), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
        pedirListaCanciones()
        
    }
    
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        
////        refrescarListaCanciones()
//        pedirListaCanciones()
//        
//    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
//        // Auriculares headphones controll
//        let route = AVAudioSession.sharedInstance().currentRoute
//        for port in route.outputs {
//            if port.portType != AVAudioSessionPortHeadphones {
//                alerta.mostrarAlerta(targetVC: self, titulo: "Auriculares", mensaje: "Para conseguir buenos resultados conecte unos auriculares")
//            }
//        }

        
        
        NotificationCenter.default.removeObserver(self, name: .UIApplicationWillEnterForeground, object: nil)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return canciones.count
    }
    
    
    


    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {

        if (canciones.count > 4){ nuRowsCanciones = canciones.count}
        if (canciones.count > 8){ nuRowsCanciones = 8}
        
        
                nuRowsCanciones = 4
        let hcell = round((self.view.frame.size.height-10)/CGFloat(nuRowsCanciones))
        
        return hcell
        
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : CancionCell
        if nuRowsCanciones == 4{
            cell = tableView.dequeueReusableCell(withIdentifier: "CancionCell4", for: indexPath) as! CancionCell
        }else{
            cell = tableView.dequeueReusableCell(withIdentifier: "CancionCell", for: indexPath) as! CancionCell
            
        }
        
        let cancion = canciones[indexPath.row]
        
        self.configurarCelda(cell, conCancion:cancion,indexPath)
        
        return cell
        
    }

    
    func configurarCelda(_ cell:CancionCell, conCancion cancion:Cancion, _ indice:IndexPath){
    
        if let titulo = cancion.tema {
            cell.tituloCancion.text = titulo
        }
        
        if let artista = cancion.autor {
            cell.nombreArtista.text = artista
        }
        
        if var caratula = cancion.imagenMini {
            
            caratula = "http://\(caratula)"
            cell.caratulaAutor.kf.setImage(with: ImageResource(downloadURL: URL(string:caratula)!), placeholder: #imageLiteral(resourceName: "image003"), options: nil, progressBlock: nil, completionHandler: nil)
            
            
        }
        
        if var urlCancion = cancion.urlAudioMini {
        
            urlCancion = "http://\(urlCancion)"
            cell.urlCancion = urlCancion
        
        }
        
        cell.botonCantar.tag = indice.row
//        cell.botonCantar.addTarget(self, action: #selector(hacerSegue(boton:)), for: .touchUpInside)
        cell.cancion = cancion
        cell.selectionStyle = .none
        cell.delegate = self
        
        
    }
    
    
    
    // MARK: - Delegate CancionCell
    
    func test() {
        
        //        let audioUrl = NSURL(string: "http://81.45.66.119:6543/vod/mp4:rafael_mi_gran_noche_preview.mp4/playlist.m3u8")
        //        playerAudio = AVPlayer(url: audioUrl! as URL)
        //     self.playerAudio.play()
        
        
        
    }
    
    func currPlay(currAVPlayer: AVPlayer){
        
        currentAVPlayer = currAVPlayer
    }
    
    
    func cantarWasTuoched(cancion : Cancion) {
        
        cancionSeleccionada = cancion
        
        // test upload
//        var wasUploaded = false
//        dataProvider.upLoadVideo(idUsuario: self.currIdUsuario, idCancion: cancion.idCancion!, urlCancionOrig: "UrlBase", urlCancionUser: "urlrec", estado1: "1"){ estadoUpload in
//            
//            if let estadoUpload = estadoUpload {
//
//               // falta control estado
//                print (estadoUpload)
//                wasUploaded = true
//        
//            }else{
//                self.alerta.mostrarAlertaSinServicio(targetVC: self)
//
//        }
//        
//    }
    
    
    
    
        
        
        
        dataProvider.ExisteVideo(usuario: self.currIdUsuario, cancion: cancion.idCancion!, remoteHandler : { isCasting in
            
            if let isCasting = isCasting{
                
                print (isCasting)
                if (isCasting == "NO"){
                    self.performSegue(withIdentifier: "videosSegue", sender: self)
                    self.uploadAllowed  = true

                    
                }else{
                   self.alerta.mostrarAlerta(targetVC: self, titulo: "Canción subida", mensaje: "No puedes volver a subirla.")
                    
                    
                    self.uploadAllowed  = false

                    
                    
                }
                
                
            }else{
                
                self.alerta.mostrarAlertaSinServicio(targetVC: self)
                
            }
            
            
        })

    }

    
    
    // MARK: - Actions
   
    func hacerSegue(boton:UIButton){
        
        if  UserDefaults.standard.bool(forKey: "SoundActive"){
            return
            
        }
        self.performSegue(withIdentifier: "videosSegue", sender: boton)
    }
    
    
    
    @IBAction func backAction(_ sender: Any) {
        //dismiss(animated: false, completion: nil)
        if  let _paso : AVPlayer    = currentAVPlayer
        {
            _paso.pause()
            
        }
        
        
        
        self.performSegue(withIdentifier: "logout", sender: self)
    }
    
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "videosSegue" {
            let cameraVC = segue.destination as! CameraViewController
                if existeConexionInternet(){
                    cameraVC.cancion = cancionSeleccionada
                    cameraVC.uploadAllowed = self.uploadAllowed

                    if cancionSeleccionada.concurso == "1" {
                    }else{
                        cameraVC.uploadAllowed = false
                        alerta.mostrarAlerta(targetVC: self, titulo: "Fuera de concurso", mensaje: "Esta canción no permite subir subir tu grabación")
                    }
                    
                }else {
                
                    alerta.mostrarAlertaSinInternet(targetVC: self)
                
                }
                
                
                
        
        
        }
        
    }
    
    
    func refrescarListaCanciones(){
    
        if comprobarFechaPeticionCanciones() {
        
            let fechaActual = obtenerFechaActualSistema()
            
            //Actualizar la fecha
            let userDefault = UserDefaults.standard
            userDefault.set(fechaActual, forKey: "fechaPeticionCanciones")
            
            pedirListaCanciones()
        
        }
    
    }
    
    func pedirListaCanciones() {
        
        if (existeConexionInternet()){
            
            cargarDatosCanciones()
            
        }else {
            
            alerta.mostrarAlertaSinInternet(targetVC: self)
        
        }
        
    }
    
    func cargarDatosCanciones(){
        
        //Si activo = nil se quita de la llamada al servicio
        
        alerta.iniciarLoading(targetVC: self, mensaje: "")
        dataProvider.getListaCanciones(activo: "", dispositivo: "IOS") { canciones in
            
            
            self.alerta.pararLoading(targetVC: self)
            if let canciones = canciones {
                self.canciones = canciones
                DispatchQueue.main.async {
                    
                    
                    
                    self.tableView .reloadData()
                    self.fijarTextoNumeroSemana(textoSemana: canciones.last?.var1)
                    
                }
                
            }else{
                
                self.alerta.mostrarAlertaSinServicio(targetVC: self)
            }
            
        }
        
    }
    
    func fijarTextoNumeroSemana(textoSemana:String?){
        
//        if let texto = textoSemana{
//            textoNumeroSemana.text = texto
//        }
        
        
    }

    func comprobarFechaPeticionCanciones() -> Bool {
    
        var esNecesarioPedirCanciones : Bool
        
        //1. Coger la fecha del sistema con formato dd.MM.aaaa
        let fechaActual = obtenerFechaActualSistema()
        
        //2. Coger la fecha guardada en el UserDefault
        if let fechaGuardada = UserDefaults.standard.object(forKey: "fechaPeticionCanciones") as? String {
        
            esNecesarioPedirCanciones = compararFechas(fechaActual: fechaActual, fechaGuardada: fechaGuardada)
        
        }else{
        
            esNecesarioPedirCanciones = true
        
        }
        
        return esNecesarioPedirCanciones
    }
    
    func obtenerFechaActualSistema() -> String {
        
        let fechaSistema = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        return formatter.string(from: fechaSistema)
        
    }
    
    func compararFechas (fechaActual: String, fechaGuardada: String) -> Bool {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let fechaActualFormatoDate = formatter.date(from: fechaActual)!
        let fechaGuardadaFormatoDate = formatter.date(from: fechaGuardada)!
        
        if fechaGuardadaFormatoDate < fechaActualFormatoDate {
            return true
        } else if fechaGuardadaFormatoDate == fechaActualFormatoDate{
            return false
        }else{
            return true
        }
    
    }
    
    
    func existeConexionInternet() -> Bool {
        
        let networkStatus = Reachability().connectionStatus()
        switch networkStatus {
        case .Unknown, .Offline:
            return false
        case .Online(.WWAN):
            print("Connected via WWAN")
            return true
        case .Online(.WiFi):
            print("Connected via WiFi")
            return true
        }
    }
    
    
    
    // delegate
//    func test
    

}
