//
//  TestViewController.swift
//  AVCam Swift
//
//  Created by Alejandro Arce on 7/12/16.
//  Copyright © 2016 Apple, Inc. All rights reserved.
//

import UIKit

class TestViewController: UIViewController {

    @IBOutlet weak var urlText: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
       

     
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func urlAction(_ sender: Any) {
        
        urlText.text = "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"
        
    }
    @IBAction func urlRafaelAction(_ sender: Any) {
        urlText.text = "http://81.45.66.119:6543/vod/mp4:sample.mp4/playlist.m3u8"
 
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        
        if (segue.identifier == "paso")
        {
            if let destinationVC = segue.destination as? CameraViewController  {
                destinationVC.resourceUrlString = urlText.text as NSString!
            }
        }
        
        
        
        
        
        
        //            segue.destination.title = String(  currentOrderHead.orderNum)
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
