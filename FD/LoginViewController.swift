//
//  LoginViewController.swift
//  FD
//
//  Created by Juan francisco Cordoba on 21/1/17.
//  Copyright © 2017 keekorok. All rights reserved.
//

import UIKit
import AVFoundation

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var textoEmailUsuario: UITextField!
    @IBOutlet weak var textoPassUsuario: UITextField!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    let dataProvider = ConnectionLocalService()
    
    var emailUsuario = String()
    var passUsuario = String()
    var currIdUsuario = String()
    let alerta = Alerta()
    
    
    
    
    var fileName : NSString!
    var filePath : NSURL!
    var playerBase : AVPlayer!
    var playerText : AVPlayer!
    var playerLayer : AVPlayerLayer!
    var playerLayerText : AVPlayerLayer!


    override func viewDidLoad() {
        super.viewDidLoad()

        /*
         Check video authorization status. Video access is required and audio
         access is optional. If audio access is denied, audio is not recorded
         during movie recording.
         */

        
        switch AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) {
        case .authorized:
            // The user has previously granted access to the camera.
            break
            
        case .notDetermined:
            /*
             The user has not yet been presented with the option to grant
             video access. We suspend the session queue to delay session
             setup until the access request has completed.
             
             Note that audio access will be implicitly requested when we
             create an AVCaptureDeviceInput for audio during session setup.
             */
            sessionQueue.suspend()
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { [unowned self] granted in
                if !granted {
                    self.setupResult = .notAuthorized
                }
                self.sessionQueue.resume()
            })
            
        default:
            // The user has previously denied access.
            setupResult = .notAuthorized
        }

        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UserDefaults.standard.set(false, forKey: "SoundActive")
        
        if (textoPassUsuario.text?.characters.count) != nil {
            textoPassUsuario.text = ""
        }
        if (textoEmailUsuario.text?.characters.count) != nil {
            textoEmailUsuario.text = ""
        }

        
        
        //camera
        //        previewView.frame = CGRect(x: 100, y: 0, width: 100, height: 100)
        
        sessionQueue.async {
            switch self.setupResult {
            case .success:
                // Only setup observers and start the session running if setup succeeded.
//                self.addObservers()
                self.session.startRunning()
                self.isSessionRunning = self.session.isRunning
                
            case .notAuthorized:
                DispatchQueue.main.async { [unowned self] in
                    let message = NSLocalizedString("AVCam doesn't have permission to use the camera, please change privacy settings", comment: "Alert message when the user has denied access to the camera")
                    let alertController = UIAlertController(title: "AVCam", message: message, preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil))
                    alertController.addAction(UIAlertAction(title: NSLocalizedString("Settings", comment: "Alert button to open Settings"), style: .`default`, handler: { action in
                        UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
                    }))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
            case .configurationFailed:
                DispatchQueue.main.async { [unowned self] in
                    let message = NSLocalizedString("Unable to capture media", comment: "Alert message when something goes wrong during capture session configuration")
                    let alertController = UIAlertController(title: "AVCam", message: message, preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
   
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        
        // micro headphones controll
        switch AVAudioSession.sharedInstance().recordPermission() {
        case AVAudioSessionRecordPermission.granted:
            print("Permission granted")
        case AVAudioSessionRecordPermission.denied:
            print("Pemission denied")
            let alertController = UIAlertController(title: "Sin acceso al micrófono", message: "No tiene permiso para usar el micrófono. Cambie en ajustes", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)

        case AVAudioSessionRecordPermission.undetermined:
            print("Request permission here")
            AVAudioSession.sharedInstance().requestRecordPermission({(granted: Bool)-> Void in
                if granted {
                    print("granted")
                } else{
                    print("not granted")
                }
            })
            
        default:
            break
        }
    }
    
    
    
    
    
    
    
    // MARK: Session Management
    
    private enum SessionSetupResult {
        case success
        case notAuthorized
        case configurationFailed
    }
    
    private let session = AVCaptureSession()
    
    private var isSessionRunning = false
    
    private let sessionQueue = DispatchQueue(label: "session queue", attributes: [], target: nil) // Communicate with the session and other session objects on this queue.
    
    private var setupResult: SessionSetupResult = .success

    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didLogout(segue:UIStoryboardSegue){
    
    
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        
//        UserDefaults.standard.set(false, forKey: "SoundActive")
//
//        if (textoPassUsuario.text?.characters.count) != nil {
//            textoPassUsuario.text = ""
//        }
//        if (textoEmailUsuario.text?.characters.count) != nil {
//            textoEmailUsuario.text = ""
//        }
//    }
    
    @IBAction func actionIrListadoCanciones(_ sender: UIButton) {
        
        if (existeConexionInternet()){
        
            obtenerDatosUsuario()
        
        }else{
        
            self.alerta.mostrarAlertaSinInternet(targetVC: self)
        
        }

        
    }
    
    
    func obtenerDatosUsuario(){
    
        textoEmailUsuario.text = "test@gmail.com"
        textoPassUsuario.text = "a1234567"
        
        self.alerta.iniciarLoading(targetVC: self,mensaje: "Comprobando...")
//        iniciarLoading()
        dataProvider.realizarLogin(email: textoEmailUsuario.text!, pass: textoPassUsuario.text!, tipoAccion:"Login") { usuario in
            
            self.alerta.pararLoading(targetVC: self)
            if let usuario = usuario {
                if let codigo = usuario.codigoCtr {
                    self.currIdUsuario = usuario.idUsuario!
                    self.comprobarCodigoLogin(codigo: codigo)
                }else{
                  self.alerta.mostrarAlertaSinServicio(targetVC: self)
                }
            }else{
                self.alerta.mostrarAlertaSinServicio(targetVC: self)
            }
            
        }

    }
    
    
    func comprobarCodigoLogin (codigo:String) {
        
        switch codigo {
            
        case "Registered":
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "ListadoVC") as! CancionesViewController
            
            newViewController.currIdUsuario = currIdUsuario
            self.present(newViewController, animated: true, completion: nil)
            
        case "InvalidPass":
            self.alerta.mostrarAlerta(targetVC: self, titulo: "Error", mensaje: "Contraseña inválida")
            
        case "NotRegister":
            self.alerta.mostrarAlerta(targetVC: self, titulo: "Error", mensaje: "Usuario no registrado. Debe registrarse primero.")
        
        case "NeedRegister":
            self.alerta.mostrarAlerta(targetVC: self, titulo: "Error", mensaje: "Usuario no registrado. Debe registrarse primero.")
        
        default:
            self.alerta.mostrarAlerta(targetVC: self, titulo: "Error", mensaje: "Ha ocurrido un error")
        }
    
    }
    
    
    func existeConexionInternet() -> Bool {
        
        let networkStatus = Reachability().connectionStatus()
        switch networkStatus {
        case .Unknown, .Offline:
            return false
        case .Online(.WWAN):
            print("Connected via WWAN")
            return true
        case .Online(.WiFi):
            print("Connected via WiFi")
            return true
        }
    }
    
    
    //MARK: UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if let texto = textField.text {
            
            if texto.characters.count == 0 {
                
                cambiarColorBordeDefecto(textField)
                
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        switch textField.tag {
        case 1:
            
            if let texto = textField.text {
                
                if texto.characters.count == 0 {
                    
                    cambiarColorBordeDefecto(textField)
                    
                }
            }
            
        default:
            
            cambiarColorBordeDefecto(textField)
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text: NSString = (textField.text ?? "") as NSString
        let resultString = text.replacingCharacters(in: range, with: string)
        
        switch textField.tag {
        case 1:
            if !esEmailValido(emailAddressString: resultString) {
                cambiarColorBordeError(textField)
            }else{
                cambiarColorBordeDefecto(textField)
            }
                    
        default:
            textField.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).cgColor
        }
        
        return true
    }

    
    
    func esEmailValido(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    
    //MARK: Métodos
    func cambiarColorBordeError(_ textfield: UITextField) {
        textfield.layer.borderWidth = 1.0
        textfield.layer.cornerRadius = 5.0
        textfield.layer.borderColor = UIColor.red.cgColor
    }
    
    func cambiarColorBordeDefecto(_ textfield: UITextField) {
        
        textfield.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).cgColor
        
    }
    
    func iniciarLoading(){
        
        loading.isHidden = false
        loading.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    
    func pararLoading(){
        
        loading.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
        
    }
}




#if PERMISSION_MICROPHONE
import AVFoundation

internal extension Permission {
    var statusMicrophone: PermissionStatus {
        let status = AVAudioSession.sharedInstance().recordPermission()
        
        switch status {
        case AVAudioSessionRecordPermission.denied:  return .denied
        case AVAudioSessionRecordPermission.granted: return .authorized
        default:                                     return .notDetermined
        }
    }
    
    func requestMicrophone(_ callback: @escaping Callback) {
        AVAudioSession.sharedInstance().requestRecordPermission { _ in
            callback(self.statusMicrophone)
        }
    }
}
#endif
