//
//  ConnectionLocalService.swift
//  FD
//
//  Created by Juan francisco Cordoba on 20/12/16.
//  Copyright © 2016 keekorok. All rights reserved.
//

import Foundation

class ConnectionLocalService {


    let remoteService = ConnectionRemoteService()
    
    
    func ExisteVideo(usuario: String, cancion: String, remoteHandler: @escaping (String?) -> Void){
        remoteService.ExisteVideo(usuario: usuario, cancion: cancion) { isCasting in
            if let isCasting = isCasting {
                remoteHandler(isCasting)
            }else{
                print("Error mientras llama al servicio REST")
                remoteHandler(nil)
            }
        }
    }
    
    func realizarLogin (email:String, pass:String, tipoAccion:String, remoteHandler: @escaping (LoginResponseDatos?) -> Void){
    
        remoteService.realizarLogin(usuario: email, pass: pass, tipoAccion: tipoAccion) { usuario in
            
            if let usuario = usuario {
            
                let datosUsuario = LoginResponseDatos(codigoCtr: usuario["codigoCtr"], idUsuario: usuario["idUsuario"], email: usuario["email"], pass: usuario["pass"], telefono: usuario["telefono"], nombre: usuario["nombre"], apellidos: usuario["apellidos"], edad: usuario["edad"], ciudad: usuario["ciudad"])
                
                remoteHandler(datosUsuario)
            
            }else{
                
                print("Error mientras llama al servicio REST")
                remoteHandler(nil)
            
            }
        
        
        
        }
    
    
    
    }
    
    func realizarRegistro (email:String, pass:String, telefono:String, nombre:String, apellidos:String, ciudad:String, edad:String, tipoAccion:String, remoteHandler: @escaping (LoginResponseDatos?) -> Void){
    
        remoteService.realizarRegistro(usuario: email, pass: pass, telefono: telefono, nombre: nombre, apellidos: apellidos, ciudad: ciudad, edad: edad, tipoAccion: tipoAccion){ usuario in
            
            if let usuario = usuario {
                
                let datosUsuario = LoginResponseDatos(codigoCtr: usuario["codigoCtr"], idUsuario: usuario["idUsuario"], email: usuario["email"], pass: usuario["pass"], telefono: usuario["telefono"], nombre: usuario["nombre"], apellidos: usuario["apellidos"], edad: usuario["edad"], ciudad: usuario["ciudad"])
                
                remoteHandler(datosUsuario)
                
            }else{
                
                print("Error mientras llama al servicio REST")
                remoteHandler(nil)
                
            }
        
        
        }
    
    
    
    }
    
    
    func getListaCanciones (activo: String?, dispositivo: String, remoteHandler: @escaping ([Cancion]?) -> Void){
    
        remoteService.getListaCanciones(activo: activo, dispositivo: dispositivo) { canciones in
        
            if let canciones = canciones {
            
                var modelCanciones = [Cancion]()
                for cancion in canciones {
                
                    let modelCancion = Cancion(idCancion: cancion["idCancion"], activo: cancion["activo"], concurso: cancion["concurso"], imagenMini: cancion["imagenMini"], urlVideoBase: cancion["urlVideoBase"], urlVideoTexto: cancion["urlVideoTexto"], urlAudioMini: cancion["urlAudioMini"], autor: cancion["autor"], tema: cancion["tema"], var1: cancion["var1"], var2: cancion["var2"], var3: cancion["var3"])
                    
                    modelCanciones.append(modelCancion)
                
                }
            
                remoteHandler(modelCanciones)
        
            }else{
                
                print("Error mientras llama al servicio REST")
                remoteHandler(nil)
            
            }
        
        
        }
    
        
    }

    func upLoadVideo(idUsuario:String, idCancion:String, urlCancionOrig:String,  urlCancionUser:String, estado1:String, remoteHandler: @escaping  (String?) -> Void){
        
        remoteService.upLoadVideo(idUsuario: idUsuario, idCancion: idCancion, urlCancionOrig: urlCancionOrig, urlCancionUser: urlCancionUser, estado1: estado1){ isUpload in
            if let isUpload = isUpload {
                remoteHandler(isUpload)
            }else{
                print("Error mientras llama al servicio REST")
                remoteHandler(nil)
            }
            
        }
        
    }
    
        
        
//        { casting in
//            
//            if let casting = casting{
//                
//                let datosCasting = Casting(idUsuario: casting["idUsuario"], idCancion: casting["idCancion"], urlCancionOrig: casting["urlCancionOrig"], urlCancionUser: casting["urlCancionUser"], estado1: casting["estado1"])
//                remoteHandler (datosCasting)
//            }else{
//                    
//                    print("Error mientras llama al servicio REST")
//                    remoteHandler(nil)
//
//                }
//                
//            }
//        
//    }

 
    
    
    

}
