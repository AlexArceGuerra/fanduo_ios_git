/*
	Copyright (C) 2016 Apple Inc. All Rights Reserved.
	See LICENSE.txt for this sample’s licensing information
	
	Abstract:
	View controller for camera interface.
*/



import UIKit
import AVFoundation
import Photos

class CameraViewController: UIViewController, AVCaptureFileOutputRecordingDelegate,DuoViewViewControllerDelegate {
	// MARK: View Controller Life Cycle
    @IBOutlet weak var viewBase: UIView!
    @IBOutlet weak var viewText: UIView!
    
    
    let alerta = Alerta()

    var fileName : NSString!
    var filePath : NSURL!
    var playerBase : AVPlayer!
    var playerText : AVPlayer!
    var playerLayer : AVPlayerLayer!
    var playerLayerText : AVPlayerLayer!

    
    var resourceUrlString : NSString!
    
    var cancion:Cancion?
    var uploadAllowed  : Bool!
    
    
    //let notificationName = Notification.Name("UIApplicationDidEnterBackgroundNotification")

    let notificationName = Notification.Name("UIApplicationWillResignActiveNotification")
    
    
    @IBAction func exitAction(_ sender: Any) {
        
  
    dismiss(animated: true, completion: nil)
        
    }
    
    @IBOutlet weak var playDuoButton: UIButton!
    @IBAction func cargaLayerAction(_ sender: Any) {
        
        let playerLayer = AVPlayerLayer(player: playerBase)
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        playerLayer.frame = viewBase.bounds
        viewBase.layer.addSublayer(playerLayer)

        
    }
    
    
    
    override func viewDidLoad() {
		super.viewDidLoad()
		
		cameraButton.isEnabled = false
		recordButton.isEnabled = false
		captureModeControl.isEnabled = false
		
		// Set up the video preview view.
		previewView.session = session
        // Alex
        previewView.videoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
    
		sessionQueue.async { [unowned self] in
			self.configureSession()
        }
        

		
	}
	
    
    override func viewDidDisappear(_ animated: Bool) {
//        NotificationCenter.default.removeObserver(self, name: notificationName, object: nil);

        
    }
    
    
    func myBackGroudControl()  {
        
        self.dismiss(animated: false, completion: nil)

    }
    
    
    //delegate DuoViewViewControllerDelegate
    func uploadEnd() {
        
        self.dismiss(animated: false, completion: nil)
        
        
    }
        
        
        
        
    override func viewDidAppear(_ animated: Bool) {
        //alex
        
        var urlVideoBase = String()
        var urlVideoTexto = String()
        
        if let cancion = cancion {
        
            urlVideoBase = "http://\(cancion.urlVideoBase!)"
            urlVideoTexto = "http://\(cancion.urlVideoTexto!)"
            let resourceUrl = NSURL(string: urlVideoBase)
            
            // video base
            playerBase = AVPlayer(url: resourceUrl! as URL)
            playerLayer = AVPlayerLayer(player: playerBase)
            playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
            playerLayer.frame = viewBase.bounds
            viewBase.layer.addSublayer(playerLayer)
            
            
            
            let resourceUrlText = NSURL(string: urlVideoTexto)
            playerText = AVPlayer(url: resourceUrlText! as URL)
            playerLayerText = AVPlayerLayer(player: playerText)
            playerLayerText.videoGravity = AVLayerVideoGravityResizeAspectFill;
            playerLayerText.frame = viewText.bounds
            viewText.layer.addSublayer(playerLayerText)
            
            
            
           //  notifications
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(playerDidFinishPlaying),
                name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                object: playerBase.currentItem)
            
            
            
//            playerBase!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, 1), queue: DispatchQueue.main) { (CMTime) -> Void in
//                print ("obb")
//                if self.playerBase!.currentItem?.status == .readyToPlay {
//                    print ("readyToPlay")
//                    
//                }
//            }
//
//            
//            playerBase = AVPlayer(url: resourceUrl! as URL)

            
            
//            playerBase.currentItem!.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions(), context: &sessionRunningObserveContext)
//            playerText.currentItem!.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions(), context: &sessionRunningObserveContext)
            
            
            // alex chage camera front
            self.changeCamera(self.cameraButton)
            playDuoButton.isHidden = true
            
            
            // Auriculares headphones controll
            let route = AVAudioSession.sharedInstance().currentRoute
            for port in route.outputs {
                if port.portType != AVAudioSessionPortHeadphones {
                    alerta.mostrarAlerta(targetVC: self, titulo: "Auriculares", mensaje: "Para conseguir buenos resultados conecte unos auriculares")
                }
            }
        }
        
    }
    
    
    func playerDidFinishPlaying(note: NSNotification) {
        print("Video Finished")
        self .toggleMovieRecording(recordButton)
        self.recordButton.setImage(#imageLiteral(resourceName: "Play Record"), for: UIControlState.normal)
        playDuoButton.isHidden = false
        

    }
    
  
    

    
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
        
        
        
        //camera
//        previewView.frame = CGRect(x: 100, y: 0, width: 100, height: 100)
        
		sessionQueue.async {
			switch self.setupResult {
                case .success:
				    // Only setup observers and start the session running if setup succeeded.
                    self.addObservers()
                    self.session.startRunning()
                    self.isSessionRunning = self.session.isRunning
				
                case .notAuthorized:
                    DispatchQueue.main.async { [unowned self] in
                        let message = NSLocalizedString("AVCam doesn't have permission to use the camera, please change privacy settings", comment: "Alert message when the user has denied access to the camera")
                        let alertController = UIAlertController(title: "AVCam", message: message, preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil))
                        alertController.addAction(UIAlertAction(title: NSLocalizedString("Settings", comment: "Alert button to open Settings"), style: .`default`, handler: { action in
                            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
                        }))
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
				
                case .configurationFailed:
                    DispatchQueue.main.async { [unowned self] in
                        let message = NSLocalizedString("Unable to capture media", comment: "Alert message when something goes wrong during capture session configuration")
                        let alertController = UIAlertController(title: "AVCam", message: message, preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil))
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
			}
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		sessionQueue.async { [unowned self] in
			if self.setupResult == .success {
				self.session.stopRunning()
                
                
                //alex
				self.isSessionRunning = self.session.isRunning
				
                
                
                
                self.playerBase.pause()
                self.playerText.pause()
                
                self.playerLayer .removeFromSuperlayer()
                self.playerLayerText .removeFromSuperlayer()
                
                self.removeObservers()
			}
		}
		
        
//        self.removeObservers()
//
//        playerBase.pause()
//        playerText.pause()
//        
//        playerLayer .removeFromSuperlayer()
//        playerLayerText .removeFromSuperlayer()

        
		super.viewWillDisappear(animated)
	}
	
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        
        if segue.identifier == "videoPlayer" {
            
            let duoVC = segue.destination as! DuoViewViewController
            duoVC.delegate = self
           
//            if  let contrUpload = !uploadAllowed {
                duoVC.songUploadAllowed = self.uploadAllowed
//            }
            
            if let button = sender as? UIButton {
                
                let cancionSeleccionada = cancion
                if cancionSeleccionada?.concurso == "1" {
                    duoVC.cancion = cancionSeleccionada
                }else{
                    
                    //                    alerta.mostrarAlerta(targetVC: self, titulo: "Aviso", mensaje: "No hay video disponible para grabar")
                    
                }
                
            }
            
        }
        
    }

    override var shouldAutorotate: Bool {
		// Disable autorotation of the interface when recording is in progress.
		if let movieFileOutput = movieFileOutput {
			return !movieFileOutput.isRecording
		}
		return true
	}
	
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
		return .all
	}
	
	override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
		super.viewWillTransition(to: size, with: coordinator)
		
		if let videoPreviewLayerConnection = previewView.videoPreviewLayer.connection {
			let deviceOrientation = UIDevice.current.orientation
			guard let newVideoOrientation = deviceOrientation.videoOrientation, deviceOrientation.isPortrait || deviceOrientation.isLandscape else {
				return
			}
			
			videoPreviewLayerConnection.videoOrientation = newVideoOrientation
		}
	}

	// MARK: Session Management
	
	private enum SessionSetupResult {
		case success
		case notAuthorized
		case configurationFailed
	}
	
	private let session = AVCaptureSession()
	
	private var isSessionRunning = false
	
	private let sessionQueue = DispatchQueue(label: "session queue", attributes: [], target: nil) // Communicate with the session and other session objects on this queue.
	
	private var setupResult: SessionSetupResult = .success
	
	var videoDeviceInput: AVCaptureDeviceInput!
	
	@IBOutlet private weak var previewView: PreviewView!
	
	// Call this on the session queue.
	private func configureSession() {
		if setupResult != .success {
			return
		}
		
		session.beginConfiguration()
		
		/*
			We do not create an AVCaptureMovieFileOutput when setting up the session because the
			AVCaptureMovieFileOutput does not support movie recording with AVCaptureSessionPresetPhoto.
		*/
		session.sessionPreset = AVCaptureSessionPresetPhoto
		
		// Add video input.
		do {
			var defaultVideoDevice: AVCaptureDevice?
			
			// Choose the back dual camera if available, otherwise default to a wide angle camera.
			if let dualCameraDevice = AVCaptureDevice.defaultDevice(withDeviceType: .builtInDuoCamera, mediaType: AVMediaTypeVideo, position: .back) {
				defaultVideoDevice = dualCameraDevice
			}
			else if let backCameraDevice = AVCaptureDevice.defaultDevice(withDeviceType: .builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: .back) {
				// If the back dual camera is not available, default to the back wide angle camera.
				defaultVideoDevice = backCameraDevice
			}
			else if let frontCameraDevice = AVCaptureDevice.defaultDevice(withDeviceType: .builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: .front) {
				// In some cases where users break their phones, the back wide angle camera is not available. In this case, we should default to the front wide angle camera.
				defaultVideoDevice = frontCameraDevice
			}
			
            // alex  po defaultVideoDevice?.formats
			let videoDeviceInput = try AVCaptureDeviceInput(device: defaultVideoDevice)
			
			if session.canAddInput(videoDeviceInput) {
				session.addInput(videoDeviceInput)
				self.videoDeviceInput = videoDeviceInput
				
				DispatchQueue.main.async {
					/*
						Why are we dispatching this to the main queue?
						Because AVCaptureVideoPreviewLayer is the backing layer for PreviewView and UIView
						can only be manipulated on the main thread.
						Note: As an exception to the above rule, it is not necessary to serialize video orientation changes
						on the AVCaptureVideoPreviewLayer’s connection with other session manipulation.
					
						Use the status bar orientation as the initial video orientation. Subsequent orientation changes are
						handled by CameraViewController.viewWillTransition(to:with:).
					*/
					let statusBarOrientation = UIApplication.shared.statusBarOrientation
					var initialVideoOrientation: AVCaptureVideoOrientation = .portrait
					if statusBarOrientation != .unknown {
						if let videoOrientation = statusBarOrientation.videoOrientation {
							initialVideoOrientation = videoOrientation
						}
					}
					
					self.previewView.videoPreviewLayer.connection.videoOrientation = initialVideoOrientation
				}
			}
			else {
				print("Could not add video device input to the session")
				setupResult = .configurationFailed
				session.commitConfiguration()
				return
			}
		}
		catch {
			print("Could not create video device input: \(error)")
			setupResult = .configurationFailed
			session.commitConfiguration()
			return
		}
		
        //alex
		// Add audio input.
		do {
            let audioDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeAudio)
			let audioDeviceInput = try AVCaptureDeviceInput(device: audioDevice)
			
//            try! AVAudioSession.sharedInstance().setInputGain(0.5)
            
            
			if session.canAddInput(audioDeviceInput) {
				session.addInput(audioDeviceInput)
                
                
                
                
			}
                
                
                
                
			else {
				print("Could not add audio device input to the session")
			}
		}
		catch {
			print("Could not create audio device input: \(error)")
		}
		
        // Add Micro input.
        
        
        
//        do {
//            let audioDevice = AVCaptureDevice.defaultDevice(withMediaType: Avmedi)
//            let audioDeviceInput = try AVCaptureDeviceInput(device: audioDevice)
//            
//            if session.canAddInput(audioDeviceInput) {
//                session.addInput(audioDeviceInput)
//            }
//            else {
//                print("Could not add audio device input to the session")
//            }
//        }
//        catch {
//            print("Could not create audio device input: \(error)")
//        }

        
        
        
        
		// Add photo output.
		if session.canAddOutput(photoOutput)
		{
			session.addOutput(photoOutput)
			
			photoOutput.isHighResolutionCaptureEnabled = true
			photoOutput.isLivePhotoCaptureEnabled = photoOutput.isLivePhotoCaptureSupported
			livePhotoMode = photoOutput.isLivePhotoCaptureSupported ? .on : .off
		}
		else {
			print("Could not add photo output to the session")
			setupResult = .configurationFailed
			session.commitConfiguration()
			return
		}
		
		session.commitConfiguration()
        
        captureModeControl.selectedSegmentIndex = 1
        //Alex fuerza video
        self .toggleCaptureMode(captureModeControl!)
	}
	
//	@IBAction private func resumeInterruptedSession(_ resumeButton: UIButton)
//	{
////		sessionQueue.async { [unowned self] in
////			/*
////				The session might fail to start running, e.g., if a phone or FaceTime call is still
////				using audio or video. A failure to start the session running will be communicated via
////				a session runtime error notification. To avoid repeatedly failing to start the session
////				running, we only try to restart the session running in the session runtime error handler
////				if we aren't trying to resume the session running.
////			*/
////			self.session.startRunning()
////			self.isSessionRunning = self.session.isRunning
////			if !self.session.isRunning {
////				DispatchQueue.main.async { [unowned self] in
////					let message = NSLocalizedString("Unable to resume", comment: "Alert message when unable to resume the session running")
////					let alertController = UIAlertController(title: "AVCam", message: message, preferredStyle: .alert)
////					let cancelAction = UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil)
////					alertController.addAction(cancelAction)
////					self.present(alertController, animated: true, completion: nil)
////				}
////			}
////			else {
////				DispatchQueue.main.async { [unowned self] in
////					self.resumeButton.isHidden = true
////				}
////			}
////		}
//	}
	
	private enum CaptureMode: Int {
		case photo = 0
		case movie = 1
	}

	@IBOutlet private weak var captureModeControl: UISegmentedControl!
	
	@IBAction private func toggleCaptureMode(_ captureModeControl: UISegmentedControl) {
		if captureModeControl.selectedSegmentIndex == CaptureMode.photo.rawValue {
			recordButton.isEnabled = false
			
			sessionQueue.async { [unowned self] in
				/*
					Remove the AVCaptureMovieFileOutput from the session because movie recording is
					not supported with AVCaptureSessionPresetPhoto. Additionally, Live Photo
					capture is not supported when an AVCaptureMovieFileOutput is connected to the session.
				*/
				self.session.beginConfiguration()
				self.session.removeOutput(self.movieFileOutput)
				self.session.sessionPreset = AVCaptureSessionPresetPhoto
				self.session.commitConfiguration()
				
				self.movieFileOutput = nil
				
				if self.photoOutput.isLivePhotoCaptureSupported {
					self.photoOutput.isLivePhotoCaptureEnabled = true
					
					DispatchQueue.main.async {
//						self.livePhotoModeButton.isEnabled = true
//						self.livePhotoModeButton.isHidden = false
					}
				}
			}
		}
		else if captureModeControl.selectedSegmentIndex == CaptureMode.movie.rawValue
		{
//			livePhotoModeButton.isHidden = true
			
			sessionQueue.async { [unowned self] in
 				let movieFileOutput = AVCaptureMovieFileOutput()
                
                movieFileOutput.movieFragmentInterval = kCMTimeInvalid
                
				
				if self.session.canAddOutput(movieFileOutput) {
					self.session.beginConfiguration()
					self.session.addOutput(movieFileOutput)
					
                    //alex resolución cámara
                    self.session.sessionPreset = AVCaptureSessionPreset352x288
//                    self.session.sessionPreset = AVCaptureSessionPresetLow
                   self.session.sessionPreset = AVCaptureSessionPresetMedium

                    
					if let connection = movieFileOutput.connection(withMediaType: AVMediaTypeVideo) {
						if connection.isVideoStabilizationSupported {
							connection.preferredVideoStabilizationMode = .auto
                            
						}
					}
					self.session.commitConfiguration()
					
					self.movieFileOutput = movieFileOutput
					
					DispatchQueue.main.async { [unowned self] in
						self.recordButton.isEnabled = true
					}
				}
			}
		}
        
        
        
        
        
	}
	
	// MARK: Device Configuration
	
	@IBOutlet private weak var cameraButton: UIButton!
	
	@IBOutlet private weak var cameraUnavailableLabel: UILabel!
	
	private let videoDeviceDiscoverySession = AVCaptureDeviceDiscoverySession(deviceTypes: [.builtInWideAngleCamera, .builtInDuoCamera], mediaType: AVMediaTypeVideo, position: .unspecified)!
	
	@IBAction private func changeCamera(_ cameraButton: UIButton) {
		cameraButton.isEnabled = false
		recordButton.isEnabled = false
//		photoButton.isEnabled = false
//		livePhotoModeButton.isEnabled = false
		captureModeControl.isEnabled = false
		
		sessionQueue.async { [unowned self] in
			let currentVideoDevice = self.videoDeviceInput.device
			let currentPosition = currentVideoDevice!.position
			
            
            
			let preferredPosition: AVCaptureDevicePosition
			let preferredDeviceType: AVCaptureDeviceType
			
			switch currentPosition {
				case .unspecified, .front:
					preferredPosition = .back
					preferredDeviceType = .builtInDuoCamera
				
				case .back:
					preferredPosition = .front
					preferredDeviceType = .builtInWideAngleCamera
			}
			
			let devices = self.videoDeviceDiscoverySession.devices!
			var newVideoDevice: AVCaptureDevice? = nil
			
			// First, look for a device with both the preferred position and device type. Otherwise, look for a device with only the preferred position.
			if let device = devices.filter({ $0.position == preferredPosition && $0.deviceType == preferredDeviceType }).first {
				newVideoDevice = device
			}
			else if let device = devices.filter({ $0.position == preferredPosition }).first {
				newVideoDevice = device
			}

            if let videoDevice = newVideoDevice {
                do {
					let videoDeviceInput = try AVCaptureDeviceInput(device: videoDevice)
					
					self.session.beginConfiguration()
					
					// Remove the existing device input first, since using the front and back camera simultaneously is not supported.
					self.session.removeInput(self.videoDeviceInput)
					
					if self.session.canAddInput(videoDeviceInput) {
						NotificationCenter.default.removeObserver(self, name: Notification.Name("AVCaptureDeviceSubjectAreaDidChangeNotification"), object: currentVideoDevice!)
						
						NotificationCenter.default.addObserver(self, selector: #selector(self.subjectAreaDidChange), name: Notification.Name("AVCaptureDeviceSubjectAreaDidChangeNotification"), object: videoDeviceInput.device)
						
						self.session.addInput(videoDeviceInput)
						self.videoDeviceInput = videoDeviceInput
					}
					else {
						self.session.addInput(self.videoDeviceInput);
					}
					
					if let connection = self.movieFileOutput?.connection(withMediaType: AVMediaTypeVideo) {
						if connection.isVideoStabilizationSupported {
							connection.preferredVideoStabilizationMode = .auto
						}
					}
					
					/*
						Set Live Photo capture enabled if it is supported. When changing cameras, the
						`isLivePhotoCaptureEnabled` property of the AVCapturePhotoOutput gets set to NO when
						a video device is disconnected from the session. After the new video device is
						added to the session, re-enable Live Photo capture on the AVCapturePhotoOutput if it is supported.
					*/
					self.photoOutput.isLivePhotoCaptureEnabled = self.photoOutput.isLivePhotoCaptureSupported;
					
					self.session.commitConfiguration()
				}
				catch {
					print("Error occured while creating video device input: \(error)")
				}
			}
			
			DispatchQueue.main.async { [unowned self] in
				self.cameraButton.isEnabled = true
				self.recordButton.isEnabled = self.movieFileOutput != nil

                //				self.photoButton.isEnabled = true
//				self.livePhotoModeButton.isEnabled = true
//				self.captureModeControl.isEnabled = true
			}
		}
	}
	
	@IBAction private func focusAndExposeTap(_ gestureRecognizer: UITapGestureRecognizer) {
		let devicePoint = self.previewView.videoPreviewLayer.captureDevicePointOfInterest(for: gestureRecognizer.location(in: gestureRecognizer.view))
		focus(with: .autoFocus, exposureMode: .autoExpose, at: devicePoint, monitorSubjectAreaChange: true)
	}
	
	private func focus(with focusMode: AVCaptureFocusMode, exposureMode: AVCaptureExposureMode, at devicePoint: CGPoint, monitorSubjectAreaChange: Bool) {
		sessionQueue.async { [unowned self] in
			if let device = self.videoDeviceInput.device {
				do {
					try device.lockForConfiguration()
					
					/*
						Setting (focus/exposure)PointOfInterest alone does not initiate a (focus/exposure) operation.
						Call set(Focus/Exposure)Mode() to apply the new point of interest.
					*/
					if device.isFocusPointOfInterestSupported && device.isFocusModeSupported(focusMode) {
						device.focusPointOfInterest = devicePoint
						device.focusMode = focusMode
					}
					
					if device.isExposurePointOfInterestSupported && device.isExposureModeSupported(exposureMode) {
						device.exposurePointOfInterest = devicePoint
						device.exposureMode = exposureMode
					}
					
					device.isSubjectAreaChangeMonitoringEnabled = monitorSubjectAreaChange
					device.unlockForConfiguration()
				}
				catch {
					print("Could not lock device for configuration: \(error)")
				}
			}
		}
	}
	
	// MARK: Capturing Photos

	private let photoOutput = AVCapturePhotoOutput()
	
//	private var inProgressPhotoCaptureDelegates = [Int64 : PhotoCaptureDelegate]()
	
	
	
	private enum LivePhotoMode {
		case on
		case off
	}
	
	private var livePhotoMode: LivePhotoMode = .off
	
//	@IBOutlet private weak var livePhotoModeButton: UIButton!
	

	
	private var inProgressLivePhotoCapturesCount = 0
	
//	@IBOutlet var capturingLivePhotoLabel: UILabel!
	
	// MARK: Recording Movies
	
	private var movieFileOutput: AVCaptureMovieFileOutput? = nil
	
	private var backgroundRecordingID: UIBackgroundTaskIdentifier? = nil
	
	@IBOutlet private weak var recordButton: UIButton!
	
//	@IBOutlet private weak var resumeButton: UIButton!
	
	@IBAction private func toggleMovieRecording(_ recordButton: UIButton) {
		
        
        if (self.playerBase!.currentItem?.status != .readyToPlay) &&
            (self.playerText!.currentItem?.status != .readyToPlay)
        {

            self.recordButton.setImage(#imageLiteral(resourceName: "Play Record"), for: UIControlState.normal)

            let alertController = UIAlertController(title: "Streaming no disponible", message: "En este momento el video no esta disponible", preferredStyle: UIAlertControllerStyle.actionSheet)
            alertController.view.tintColor = UIColor(red: 1.0, green: 0.25, blue: 1.0, alpha: 1.0)

            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                return
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        
        }
        
        
        
        //alex
        let route = AVAudioSession.sharedInstance().currentRoute

        
        
        
        guard let movieFileOutput = self.movieFileOutput else {
			return
		}
		
		/*
			Disable the Camera button until recording finishes, and disable
			the Record button until recording starts or finishes.
		
			See the AVCaptureFileOutputRecordingDelegate methods.
		*/
		cameraButton.isEnabled = false
		recordButton.isEnabled = false
		captureModeControl.isEnabled = false
		
		/*
			Retrieve the video preview layer's video orientation on the main queue
			before entering the session queue. We do this to ensure UI elements are
			accessed on the main thread and session configuration is done on the session queue.
		*/
		let videoPreviewLayerOrientation = previewView.videoPreviewLayer.connection.videoOrientation
		
		sessionQueue.async { [unowned self] in
			if !movieFileOutput.isRecording {
				if UIDevice.current.isMultitaskingSupported {
					/*
						Setup background task.
						This is needed because the `capture(_:, didFinishRecordingToOutputFileAt:, fromConnections:, error:)`
						callback is not received until AVCam returns to the foreground unless you request background execution time.
						This also ensures that there will be time to write the file to the photo library when AVCam is backgrounded.
						To conclude this background execution, endBackgroundTask(_:) is called in
						`capture(_:, didFinishRecordingToOutputFileAt:, fromConnections:, error:)` after the recorded file has been saved.
					*/
					self.backgroundRecordingID = UIApplication.shared.beginBackgroundTask(expirationHandler: nil)
				}
				
				// Update the orientation on the movie file output video connection before starting recording.
				let movieFileOutputConnection = self.movieFileOutput?.connection(withMediaType: AVMediaTypeVideo)
				movieFileOutputConnection?.videoOrientation = videoPreviewLayerOrientation
				
				// Start recording to a temporary file.
                //alex
                
//                let offsetTime = scrollView.contentOffset.y * 0.1
//                let seekTime : CMTime = CMTimeMake(0, 2000)
//                self.playerBase.seek(to: seekTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
                
                
                    self.playerBase.seek(to: kCMTimeZero, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
                    self.playerText.seek(to: kCMTimeZero, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
                    
                    self.playerBase.play()
                    self.playerText.play()
                    self.viewText.isHidden = false
                    
             

                

                
                // new
                				let outputFileName = "mysavefile"
                				let outputFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent((outputFileName as NSString).appendingPathExtension("mp4")!)
                				movieFileOutput.startRecording(toOutputFileURL: URL(fileURLWithPath: outputFilePath), recordingDelegate: self)
                //old
//				let outputFileName = NSUUID().uuidString
//				let outputFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent((outputFileName as NSString).appendingPathExtension("mov")!)
//				movieFileOutput.startRecording(toOutputFileURL: URL(fileURLWithPath: outputFilePath), recordingDelegate: self)
                
                
                
                
                
                
			}
			else {
				movieFileOutput.stopRecording()
                self.playerBase.pause()
                self.playerText.pause()

                
			}
		}
	}
	
	func capture(_ captureOutput: AVCaptureFileOutput!, didStartRecordingToOutputFileAt fileURL: URL!, fromConnections connections: [Any]!) {
		// Enable the Record button to let the user stop the recording.
		DispatchQueue.main.async { [unowned self] in
			self.recordButton.isEnabled = true
			self.recordButton.setTitle(NSLocalizedString("Stop", comment: "Recording button stop title"), for: [])
//            self.Blink(animate: true)
            self.recordButton.setImage(#imageLiteral(resourceName: "Play Record Red"), for: UIControlState.normal)

            self.playDuoButton.isHidden = true
            self.cameraButton.isHidden = true
        


		}
	}
	
    func capture(_ captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAt outputFileURL: URL!, fromConnections connections: [Any]!, error: Error!) {
        DispatchQueue.main.async { [unowned self] in
           self.recordButton.isEnabled = true
            self.recordButton.setTitle(NSLocalizedString("Record", comment: "Recording button Record title"), for: [])
//            self.Blink(animate: false)
            self.recordButton.setImage(#imageLiteral(resourceName: "Play Record"), for: UIControlState.normal)

            self.playDuoButton.isHidden = false
            self.cameraButton.isHidden = false
            self.cameraButton.isEnabled = true


        }

        
        
        
        //alex // quitar return para grabar en libreria publica
        return;
        
        
        /*
			Note that currentBackgroundRecordingID is used to end the background task
			associated with this recording. This allows a new recording to be started,
			associated with a new UIBackgroundTaskIdentifier, once the movie file output's
			`isRecording` property is back to false — which happens sometime after this method
			returns.
		
			Note: Since we use a unique file path for each recording, a new recording will
			not overwrite a recording currently being saved.
		*/
        
        
        
        
        
        
		func cleanup() {
			let path = outputFileURL.path
            if FileManager.default.fileExists(atPath: path) {
                do {
                    try FileManager.default.removeItem(atPath: path)
                }
                catch {
                    print("Could not remove file at url: \(outputFileURL)")
                }
            }
			
			if let currentBackgroundRecordingID = backgroundRecordingID {
				backgroundRecordingID = UIBackgroundTaskInvalid
				
				if currentBackgroundRecordingID != UIBackgroundTaskInvalid {
					UIApplication.shared.endBackgroundTask(currentBackgroundRecordingID)
				}
			}
		}
		
		var success = true
		
		if error != nil {
			print("Movie file finishing error: \(error)")
			success = (((error as NSError).userInfo[AVErrorRecordingSuccessfullyFinishedKey] as AnyObject).boolValue)!
		}
		
		if success {
			// Check authorization status.
			PHPhotoLibrary.requestAuthorization { status in
				if status == .authorized {
					// Save the movie file to the photo library and cleanup.
					PHPhotoLibrary.shared().performChanges({
							let options = PHAssetResourceCreationOptions()
							options.shouldMoveFile = true
							let creationRequest = PHAssetCreationRequest.forAsset()
							creationRequest.addResource(with: .video, fileURL: outputFileURL, options: options)
						}, completionHandler: { success, error in
							if !success {
								print("Could not save movie to photo library: \(error)")
							}
							cleanup()
						}
					)
				}
				else {
					cleanup()
				}
			}
		}
		else {
			cleanup()
		}
		
		// Enable the Camera and Record buttons to let the user switch camera and start another recording.
		DispatchQueue.main.async { [unowned self] in
			// Only enable the ability to change camera if the device has more than one camera.
			self.cameraButton.isEnabled = self.videoDeviceDiscoverySession.uniqueDevicePositionsCount() > 1
			self.recordButton.isEnabled = true
			self.captureModeControl.isEnabled = true
			self.recordButton.setTitle(NSLocalizedString("Record", comment: "Recording button record title"), for: [])
		}
	}
	
	// MARK: KVO and Notifications
	
	private var sessionRunningObserveContext = 0
	
	private func addObservers() {
		session.addObserver(self, forKeyPath: "running", options: .new, context: &sessionRunningObserveContext)
//        playerBase.addObserver(self, forKeyPath: "status", options: .new, context: nil)
        
        
        
        
		NotificationCenter.default.addObserver(self, selector: #selector(subjectAreaDidChange), name: Notification.Name("AVCaptureDeviceSubjectAreaDidChangeNotification"), object: videoDeviceInput.device)
		NotificationCenter.default.addObserver(self, selector: #selector(sessionRuntimeError), name: Notification.Name("AVCaptureSessionRuntimeErrorNotification"), object: session)
		
		/*
			A session can only run when the app is full screen. It will be interrupted
			in a multi-app layout, introduced in iOS 9, see also the documentation of
			AVCaptureSessionInterruptionReason. Add observers to handle these session
			interruptions and show a preview is paused message. See the documentation
			of AVCaptureSessionWasInterruptedNotification for other interruption reasons.
		*/
		NotificationCenter.default.addObserver(self, selector: #selector(sessionWasInterrupted), name: Notification.Name("AVCaptureSessionWasInterruptedNotification"), object: session)
		NotificationCenter.default.addObserver(self, selector: #selector(sessionInterruptionEnded), name: Notification.Name("AVCaptureSessionInterruptionEndedNotification"), object: session)
	}
	
	private func removeObservers() {
		NotificationCenter.default.removeObserver(self)
		
		session.removeObserver(self, forKeyPath: "running", context: &sessionRunningObserveContext)
//        playerBase.currentItem!.removeObserver(self, forKeyPath: "status", context: &sessionRunningObserveContext)
//        playerText.currentItem!.removeObserver(self, forKeyPath: "status", context: &sessionRunningObserveContext)
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		
        
//        if (playerBase.currentItem?.status == AVPlayerItemStatus.readyToPlay) &&
//            (playerText.currentItem?.status == AVPlayerItemStatus.readyToPlay){
//            print("a")
//        }

//        
//        if (keyPath == "status") {
//            print("pep")
//        }
        
        
        
        
        
        if context == &sessionRunningObserveContext {
			let newValue = change?[.newKey] as AnyObject?
			guard let isSessionRunning = newValue?.boolValue else { return }
//			let isLivePhotoCaptureSupported = photoOutput.isLivePhotoCaptureSupported
//			let isLivePhotoCaptureEnabled = photoOutput.isLivePhotoCaptureEnabled
	
            
//            if let pBase = playerBase{
//                if let pText = playerText{
//                    if (pBase.currentItem?.status == AVPlayerItemStatus.readyToPlay) &&
//                        (pText.currentItem?.status == AVPlayerItemStatus.readyToPlay){
//                        print("a")
//
//                        
//                    }
//                    
//                    
//                    
//                }
//            }
            
            
            
            
            
//                    if (playerBase.currentItem?.status == AVPlayerItemStatus.readyToPlay) &&
//                        (playerText.currentItem?.status == AVPlayerItemStatus.readyToPlay){
//                        print("a")
//                    }

            
            
			DispatchQueue.main.async { [unowned self] in
				// Only enable the ability to change camera if the device has more than one camera.
				self.cameraButton.isEnabled = isSessionRunning && self.videoDeviceDiscoverySession.uniqueDevicePositionsCount() > 1
				self.recordButton.isEnabled = isSessionRunning && self.movieFileOutput != nil
				self.captureModeControl.isEnabled = isSessionRunning
//                if (self.playerBase.currentItem?.status == AVPlayerItemStatus.readyToPlay) &&
//                    (self.playerText.currentItem?.status == AVPlayerItemStatus.readyToPlay){
//                    print("a")
//                }
                
			}
		}
		else {
			super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
		}
	}
	
	func subjectAreaDidChange(notification: NSNotification) {
		let devicePoint = CGPoint(x: 0.5, y: 0.5)
		focus(with: .autoFocus, exposureMode: .continuousAutoExposure, at: devicePoint, monitorSubjectAreaChange: false)
	}
	
	func sessionRuntimeError(notification: NSNotification) {
		guard let errorValue = notification.userInfo?[AVCaptureSessionErrorKey] as? NSError else {
			return
		}
		
        let error = AVError(_nsError: errorValue)
		print("Capture session runtime error: \(error)")
		
		/*
			Automatically try to restart the session running if media services were
			reset and the last start running succeeded. Otherwise, enable the user
			to try to resume the session running.
		*/
		if error.code == .mediaServicesWereReset {
			sessionQueue.async { [unowned self] in
				if self.isSessionRunning {
					self.session.startRunning()
					self.isSessionRunning = self.session.isRunning
				}
				else {
					DispatchQueue.main.async { [unowned self] in
//						self.resumeButton.isHidden = false
					}
				}
			}
		}
		else {
//            resumeButton.isHidden = false
		}
	}
	
	func sessionWasInterrupted(notification: NSNotification) {
		/*
			In some scenarios we want to enable the user to resume the session running.
			For example, if music playback is initiated via control center while
			using AVCam, then the user can let AVCam resume
			the session running, which will stop music playback. Note that stopping
			music playback in control center will not automatically resume the session
			running. Also note that it is not always possible to resume, see `resumeInterruptedSession(_:)`.
		*/
       
        
        //alex
        
//        self.exitAction(self)
        
        
        
        
		if let userInfoValue = notification.userInfo?[AVCaptureSessionInterruptionReasonKey] as AnyObject?, let reasonIntegerValue = userInfoValue.integerValue, let reason = AVCaptureSessionInterruptionReason(rawValue: reasonIntegerValue) {
			print("Capture session was interrupted with reason \(reason)")
			
			var showResumeButton = false
			
			if reason == AVCaptureSessionInterruptionReason.audioDeviceInUseByAnotherClient || reason == AVCaptureSessionInterruptionReason.videoDeviceInUseByAnotherClient {
				showResumeButton = true
			}
			else if reason == AVCaptureSessionInterruptionReason.videoDeviceNotAvailableWithMultipleForegroundApps {
				// Simply fade-in a label to inform the user that the camera is unavailable.
				cameraUnavailableLabel.alpha = 0
				cameraUnavailableLabel.isHidden = false
				UIView.animate(withDuration: 0.25) { [unowned self] in
					self.cameraUnavailableLabel.alpha = 1
				}
			}
			
			if showResumeButton {
				// Simply fade-in a button to enable the user to try to resume the session running.
//				resumeButton.alpha = 0
//				resumeButton.isHidden = false
				UIView.animate(withDuration: 0.25) { [unowned self] in
//					self.resumeButton.alpha = 1
				}
			}
		}
	}
	
	func sessionInterruptionEnded(notification: NSNotification) {
		print("Capture session interruption ended")
        
        return
		
//		if !resumeButton.isHidden {
//			UIView.animate(withDuration: 0.25,
//				animations: { [unowned self] in
//					self.resumeButton.alpha = 0
//				}, completion: { [unowned self] finished in
//					self.resumeButton.isHidden = true
//				}
//			)
//		}
		if !cameraUnavailableLabel.isHidden {
			UIView.animate(withDuration: 0.25,
			    animations: { [unowned self] in
					self.cameraUnavailableLabel.alpha = 0
				}, completion: { [unowned self] finished in
					self.cameraUnavailableLabel.isHidden = true
				}
			)
		}
	}
}




extension UIDeviceOrientation {
    var videoOrientation: AVCaptureVideoOrientation? {
        switch self {
        case .portrait: return .portrait
        case .portraitUpsideDown: return .portraitUpsideDown
        case .landscapeLeft: return .landscapeRight
        case .landscapeRight: return .landscapeLeft
        default: return nil
        }
    }
}

extension UIInterfaceOrientation {
    var videoOrientation: AVCaptureVideoOrientation? {
        switch self {
        case .portrait: return .portrait
        case .portraitUpsideDown: return .portraitUpsideDown
        case .landscapeLeft: return .landscapeLeft
        case .landscapeRight: return .landscapeRight
        default: return nil
        }
    }
}

extension AVCaptureDeviceDiscoverySession {
    func uniqueDevicePositionsCount() -> Int {
        var uniqueDevicePositions = [AVCaptureDevicePosition]()
        
        for device in devices {
            if !uniqueDevicePositions.contains(device.position) {
                uniqueDevicePositions.append(device.position)
            }
        }
        
        return uniqueDevicePositions.count
    }
}
