//
//  Alerta.swift
//  FD
//
//  Created by Juan francisco Cordoba on 21/1/17.
//  Copyright © 2017 keekorok. All rights reserved.
//

import Foundation
import UIKit


class Alerta {

    
    
    
    
    func mostrarAlerta(targetVC: UIViewController, titulo: String, mensaje: String){
        let alert = UIAlertController(title: titulo, message: mensaje, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        alert.view.tintColor = UIColor(red: 1.0, green: 0.25, blue: 1.0, alpha: 1.0)
        targetVC.present(alert, animated: true, completion: nil)
        
    }
    
    func mostrarAlertaSinServicio (targetVC: UIViewController){
        
        mostrarAlerta(targetVC: targetVC, titulo: "El servidor está saturado", mensaje: "Inténtalo en unos minutos")
        
        
    }

    func mostrarAlertaSinInternet (targetVC: UIViewController){
        
        mostrarAlerta(targetVC: targetVC, titulo: "Sin Acceso a internet", mensaje: "Conecta WiFI/3G/4G para continuar")
        
        
    }

    func iniciarLoading(targetVC: UIViewController, mensaje: String){
        
        
        // activity
        let overlay = UIView(frame: targetVC.view.frame)
        overlay.center = targetVC.view.center
        
        overlay.alpha = 0
        overlay.backgroundColor = UIColor.black
        overlay.tag = 100
        targetVC.view.addSubview(overlay)
        targetVC.view.bringSubview(toFront: overlay)
        
        UIView .animate(withDuration: 0.5) {
            UIView.setAnimationDuration(0.5)
            overlay.alpha = 0.8
            
        }
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        indicator.center = overlay.center
        indicator.startAnimating()
        overlay.addSubview(indicator)
        let label = UILabel()
        label.text = mensaje
        label.textColor = UIColor.white
        label.sizeToFit()
        label.center = CGPoint(x: indicator.center.x, y: indicator.center.y + 30)
        overlay.addSubview(label)
        
        //        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    func pararLoading(targetVC: UIViewController){
        
        if let viewWithTag = targetVC.view.viewWithTag(100) {
            viewWithTag.removeFromSuperview()
            
        }
    }
    

}
