//
//  Casting.swift
//  FD
//
//  Created by Alejandro Arce on 10/2/17.
//  Copyright © 2017 keekorok. All rights reserved.
//

import Foundation


class Casting{
    
    let idUsuario : String?
    let idCancion : String?
    let urlCancionOrig : String?
    let urlCancionUser : String?
    let estado1 : String?
    
    
    
    
    init(idUsuario: String?, idCancion: String?, urlCancionOrig: String?, urlCancionUser: String?, estado1: String?) {
        
        self.idUsuario      = idUsuario
        self.idCancion   = idCancion
        self.urlCancionOrig  = urlCancionOrig
        self.urlCancionUser   = urlCancionUser
        self.estado1 = estado1
    }
}
