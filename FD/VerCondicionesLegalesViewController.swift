//
//  VerCondicionesLegalesViewController.swift
//  FD
//
//  Created by Juan francisco Cordoba on 3/2/17.
//  Copyright © 2017 keekorok. All rights reserved.
//

import UIKit

class VerCondicionesLegalesViewController: UIViewController, UIWebViewDelegate, UIScrollViewDelegate {

   
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var vistaContenedoraBotones: UIView!
    @IBOutlet weak var loading: UIActivityIndicatorView!

    var eleccion = Bool()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        webView.scrollView.delegate = self
        vistaContenedoraBotones.isHidden = true
        iniciarLoading()
        mostrarPdfCL()
        
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Actions
    
    @IBAction func pulsarAceptarCondiciones(_ sender: Any) {
        eleccion = true
        volverAPaginaRegistro()
    }
    
    @IBAction func pulsarRechazarCondiciones(_ sender: Any) {
        eleccion = false
        volverAPaginaRegistro()
    }
    
    
    //MARK: Metodos
    func mostrarPdfCL(){
    
        let path = NSURL(fileURLWithPath: Bundle.main.path(forResource: "Condiciones_Uso_App_180117", ofType: "pdf")!)
        let request = NSURLRequest(url:path as URL)
        webView.loadRequest(request as URLRequest)
    
    }
    
    func volverAPaginaRegistro(){
    
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "RegistroVC") as! RegistroViewController
        newViewController.aceptaCondiciones = eleccion
        newViewController.esPantallaLeerCG = true
        self.present(newViewController, animated: true, completion: nil)
    }
    
    func iniciarLoading(){
        
        loading.isHidden = false
        loading.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }

    func pararLoading(){
    
        loading.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    
    }
    
    //MARK: UIWebViewDelegate
    func webViewDidFinishLoad(_ webView: UIWebView) {
        pararLoading()
    }
    
    
    
    //MARK: UIScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height) {
        
            vistaContenedoraBotones.isHidden = false
        
        }
    }
    



}
