//
//  Cancion.swift
//  FD
//
//  Created by Juan francisco Cordoba on 20/12/16.
//  Copyright © 2016 keekorok. All rights reserved.
//

import Foundation


class Cancion{

    let idCancion : String?
    let activo : String?
    let concurso : String?
    let imagenMini : String?
    let urlVideoBase : String?
    let urlVideoTexto : String?
    let urlAudioMini : String?
    let autor : String?
    let tema : String?
    let var1 : String?
    let var2 : String?
    let var3 : String?
    
    
    
    init(idCancion: String?, activo: String?, concurso: String?, imagenMini: String?, urlVideoBase: String?, urlVideoTexto: String?, urlAudioMini: String?, autor: String?, tema: String?, var1: String?, var2: String?, var3: String?) {
        
        self.idCancion      = idCancion
        self.activo         = activo
        self.concurso       = concurso
        self.imagenMini     = imagenMini
        self.urlVideoBase   = urlVideoBase
        self.urlVideoTexto  = urlVideoTexto
        self.urlAudioMini   = urlAudioMini
        self.autor          = autor
        self.tema           = tema
        self.var1           = var1
        self.var2           = var2
        self.var3           = var3
        
    }

}
