//
//  DuoViewViewController.swift
//  AVCam Swift
//
//  Created by Alejandro Arce on 6/12/16.
//  Copyright © 2016 Apple, Inc. All rights reserved.
//











import UIKit
import AVFoundation
import AWSS3

protocol DuoViewViewControllerDelegate {
    func uploadEnd()
}



    
// configure S3
let S3BucketName = "fanduo"
// configure authentication with Cognito
let CognitoPoolID = "eu-central-1:3d84a30e-e52d-44e3-98b1-4e2c108f8733"
let Region = AWSRegionType.euCentral1
let credentialsProvider = AWSCognitoCredentialsProvider(regionType:Region,
                                                        identityPoolId:CognitoPoolID)
let configuration = AWSServiceConfiguration(region:Region, credentialsProvider:credentialsProvider)





class DuoViewViewController: UIViewController {
    @IBOutlet weak var viewBase: UIView!
    @IBOutlet weak var viewRec: UIView!
    var playerBase : AVPlayer!
    var playerBase2 : AVPlayer!
    var fileName : NSString!
    var filePath : NSURL!
    var playingDuoVideo : Bool = false
    var wasVideoUploade : Bool = false
    var allowUpload : Bool = true
    var songUploadAllowed : Bool!
    let alerta = Alerta()


    var cancion : Cancion?
    
    @IBOutlet weak var timerBar: UISlider!
//    @IBOutlet weak var timerRecBar: UIProgressView!
    
    @IBOutlet weak var playerButton: UIButton!
    
    @IBOutlet weak var upLoadButton: UIButton!
    
    let ext = "mp4"

    var delegate: DuoViewViewControllerDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        upLoadButton.isHidden = !songUploadAllowed
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        playerBase.pause()
        playerBase2 .pause()
        playingDuoVideo  = false
        wasVideoUploade = false

        
    }
    
    @IBAction func upLoadAction(_ sender: Any) {
        if !allowUpload{
            
        alerta.mostrarAlerta(targetVC: self, titulo: "No se puede subir al concurso", mensaje: "Tu interpretación es demasiado corta.")
            
//            let alertController = UIAlertController(title: "No se puede subir al concurso", message: "Tu interpretación es demasiado corta.", preferredStyle: UIAlertControllerStyle.alert)
//            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
//                print("OK")
//            }
//            
//            alertController.addAction(okAction)
//            self.present(alertController, animated: true, completion: nil)
            
        }else{
            let alertController = UIAlertController(title: "Participar en Fantastic Duo", message: "¿Estás seguro? Solo puedes participar una vez con cada artista.", preferredStyle: UIAlertControllerStyle.actionSheet)
            alertController.view.tintColor = UIColor(red: 1.0, green: 0.25, blue: 1.0, alpha: 1.0)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
                self.uploading()
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("Cancel")
                
                
            }
            
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)

        }
        
    }
    
    func  uploading()  {
        
        // activity
        let overlay = UIView(frame: self.view.frame)
        overlay.center = self.view.center
        
        overlay.alpha = 0
        overlay.backgroundColor = UIColor.black
        
        self.view.addSubview(overlay)
        self.view.bringSubview(toFront: overlay)
        
        UIView .animate(withDuration: 0.5) {
            UIView.setAnimationDuration(0.5)
            overlay.alpha = 0.8
            
        }
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        indicator.center = overlay.center
        indicator.startAnimating()
        overlay.addSubview(indicator)
        let label = UILabel()
        label.text = "Subiendo casting a Fantastic Duo"
        label.textColor = UIColor.white
        label.sizeToFit()
        label.center = CGPoint(x: indicator.center.x, y: indicator.center.y + 30)
        overlay.addSubview(label)
  
        
        
        // file  target
        let outputFileName = "mysavefile"
        let outputFilePath = (NSTemporaryDirectory() as NSString)          .appendingPathComponent((outputFileName as NSString).appendingPathExtension(ext)!)

        
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.body = NSURL(fileURLWithPath: outputFilePath) as URL!

        uploadRequest?.key = ProcessInfo.processInfo.globallyUniqueString + "." + ext
        uploadRequest?.bucket = S3BucketName
        uploadRequest?.contentType = "image/" + ext
        
        
        let transferManager = AWSS3TransferManager.default()
        
        transferManager?.upload(uploadRequest).continue({ (task) -> AnyObject! in
            if let error = task.error {
                print("Upload failed ❌ (\(error))")
            }
            if let exception = task.exception {
                print("Upload failed ❌ (\(exception))")
            }
            if task.result != nil {
                let s3File = "http://s3.amazonaws.com/" + S3BucketName + "/" + (uploadRequest?.key)!
            
                print(s3File)

                overlay.isHidden = true
                self .endUpload()
                print("Uploaded")
                self.view.backgroundColor = UIColor.red
            }
            else {
                print("Unexpected empty result.")
            }
//            overlay.isHidden = true
//            self .endUpload()

            return nil
        })

    }
    
    func endUpload()  {
        
        
        upLoadButton.isHidden = true
        wasVideoUploade = true
        
        
        let alertController = UIAlertController(title: " ! Conseguido !", message: "Ya has participado con este artista, pero hay muchos más. Prueba a grabarte con otro.", preferredStyle: UIAlertControllerStyle.actionSheet)
        alertController.view.tintColor = UIColor(red: 1.0, green: 0.25, blue: 1.0, alpha: 1.0)

        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            print("OK")
            self.dismiss(animated: false, completion: nil)

            self.delegate? .uploadEnd()
        }
        
        
        
        alertController.addAction(okAction)
    
        self.present(alertController, animated: true, completion: nil)
        

        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
//        timerRecBar.isHidden = true
        timerBar.isHidden = true
        allowUpload  = true
        
        var urlVideoBase = String()
        var seconds : Float64 = 0
        var seconds2 : Float64 = 0
        
        
        if let cancion = cancion {
            
            // videoBase
            urlVideoBase = "http://\(cancion.urlVideoBase!)"
            let resourceUrl = NSURL(string: urlVideoBase)
            let playerItem:AVPlayerItem = AVPlayerItem(url: resourceUrl! as URL)
            playerBase = AVPlayer(playerItem: playerItem)
            let playerLayer = AVPlayerLayer(player: playerBase)
            playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
            playerLayer.frame = viewBase.bounds
            viewBase.layer.addSublayer(playerLayer)
            let duration : CMTime = playerItem.asset.duration
            seconds  = CMTimeGetSeconds(duration)
            
            
            
            
            
            
            
            //videoRec
            
            fileName = "mysavefile.mp4";
            filePath = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName as String) as NSURL!
            
            let playerItem2:AVPlayerItem = AVPlayerItem(url: filePath! as URL)
            
            
            playerBase2 = AVPlayer(playerItem: playerItem2)
            let playerLayer2 = AVPlayerLayer(player: playerBase2)
            playerLayer2.videoGravity = AVLayerVideoGravityResizeAspectFill;
            
            playerLayer2.frame = viewRec.bounds
            viewRec.layer.addSublayer(playerLayer2)
            
            let duration2 : CMTime = playerItem2.asset.duration
            seconds2 = CMTimeGetSeconds(duration2)
            
            
        }else{
            
            let alertController = UIAlertController(title: "Sin  canción", message: "No se encontró una canción", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
                self.dismiss(animated: true, completion: nil)
                self.delegate? .uploadEnd()
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
            
        }
        
        
        // permite upload
        
        let diff = seconds-seconds2
        if (diff < -1 || diff > 1){
            allowUpload = false
            
        }
        upLoadButton.isHidden = !songUploadAllowed

        
        
        
        // timer bar
        timerBar.maximumValue = Float(seconds2)
        timerBar.isContinuous = true
        playerBase.seek(to: CMTimeMake(0, 1)
        )
        timerBar.addTarget(self, action: #selector(playbackSliderValueChanged(_:)), for: .valueChanged)
        
        // timerRec bar
//        timerRecBar.progress = Float(seconds2) / ( Float(seconds))
        
        
        
        // KVO
        //      KVO notification of the currentTime
        // onplay
        playerBase!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, 1), queue: DispatchQueue.main) { (CMTime) -> Void in
            print("wqwqwq")
            
            if self.playerBase2!.currentItem?.status == .readyToPlay {
                let time : Float64 = CMTimeGetSeconds(self.playerBase2.currentTime());
                self.timerBar!.value = Float ( time );

                
            }
        }
        //  end
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(playerDidFinishPlaying),
            name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
            object: playerBase2.currentItem)

        
    }
    
    func playerDidFinishPlaying(note: NSNotification) {
        
        print("Video Finished")
        
        self.playerBase.pause()
        self.playerBase2.pause()
        self.playerButton.setImage(#imageLiteral(resourceName: "Circled Play"), for: UIControlState.normal)

        
        //        movieFileOutput.stopRecording()
        
    }

    
    
    
    func playbackSliderValueChanged(_ playbackSlider:UISlider)
    {
        
        let seconds : Int64 = Int64(playbackSlider.value)
        let targetTime:CMTime = CMTimeMake(seconds, 1)
        
        
        playerBase.seek(to: targetTime)
        playerBase2.seek(to: targetTime)
        
        
        
        if playerBase!.rate == 0
        {
            playerBase?.play()
            playerBase2?.play()
            playingDuoVideo = true
            self.playerButton.setImage(#imageLiteral(resourceName: "Circled Pause"), for: UIControlState.normal)

            
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func playDuoAction(_ sender: Any) {
        
        

        
        playingDuoVideo = !playingDuoVideo
        if playingDuoVideo {
            playerBase.volume  = 1.0
            playerBase2.volume  = 1.0
            playerBase2.play()
            playerBase?.play()
            self.playerButton.setImage(#imageLiteral(resourceName: "Circled Pause"), for: UIControlState.normal)
            timerBar.isHidden = false
        }else{
            self.playerButton.setImage(#imageLiteral(resourceName: "Circled Play"), for: UIControlState.normal)
            playerBase2.pause()
            playerBase.pause()
        }
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        
        if wasVideoUploade{
            self.dismiss(animated: false, completion: nil)
            return
        }
        
        
        
        let alertController = UIAlertController(title: "Salir", message: "Se va a eliminar la actual grabación", preferredStyle: UIAlertControllerStyle.actionSheet)
        alertController.view.tintColor = UIColor(red: 1.0, green: 0.25, blue: 1.0, alpha: 1.0)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            print("OK")
           self.dismiss(animated: false, completion: nil)
        }
        
        let cancelAction = UIAlertAction(title: "cancel", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            print("Cancel")
            
            
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
        
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
