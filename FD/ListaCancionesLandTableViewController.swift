//
//  ListaCancionesLandTableViewController.swift
//  AVCam Swift
//
//  Created by Alejandro Arce on 1/1/17.
//  Copyright © 2017 Apple, Inc. All rights reserved.
//

import UIKit

class ListaCancionesLandTableViewController: UITableViewController {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 8
    }
    
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:  "HeadCell")
        return cell
        
    }
    override func  tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 0
        
    }
    override  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let hcell = round(self.view.frame.size.height/8)
        
        return hcell
        
        
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:  "Cell")
        
        
        //        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! NuevoPedidoTableViewCell
        
        //        cell.delegate = self
        //        cell.updateCell(indexRowOrderDetailFrom: indexPath.row,editable: editable)
        
        return cell!
    }

}
